/* include.h */

/*** ansi ***/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <time.h>

/*** customs ***/

#include "defines.h"
#include "types.h"

#include "allocate.h"
#include "analysis.h"
#include "bitio.h"
#include "bitmapio.h"
#include "classify.h"
#include "decode.h"
#include "encode.h"
#include "etc.h"
#include "fileio.h"
#include "findpool.h"
#include "graphics.h"
#include "mapop.h"
#include "test.h"

