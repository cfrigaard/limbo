#include "includes.h"
 
/**************************************|****************************************
"Routine   : LoadBitMap"
Input  par: char * filename (the name of the file to be loaded)
            int maxx (max xsize)
            int maxy (max ysize)
            int offx (offset x)
            int offy (offset y)
            int BlockSize (the blocksize used)
Output par: Bitmap *SourceImage (the pointer to the bitmap to be encoded)
Global var: none
Function  : Load the bitmap from file filename of the format PGM
***************************************|***************************************/
 
 BitMap *LoadBitMap(char *file,int maxx,int maxy,int offx,int offy,
                    int BlockSize,char *path)
  {
   BitMap *SourceImage,*TmpImg,*TmpImgR,*TmpImgG,*TmpImgB;
   unsigned int x,y,i,ymax,xmax,type;
   char filename[MAXFILE],temp[MAXSTRING],bitarray[MAXARRAY];
   FILE *in;
   
   vprintf(stderr,"\nLoading BitMap...");
   
   strcpy(filename,path);
   strcat(filename,file);
   
   if (!(in=fopen(filename,"rb"))) /* first try no suffix */
    {
     strcat(filename,PPMSFX);  /* try ppm */
     if (!(in=fopen(filename,"rb")))   
      {
       strcpy(filename,ORGGFX);
       strcat(filename,file);
       strcat(filename,PGMSFX); /* try pgm then */
       if (!(in=fopen(filename,"rb"))) 
        {
         ErrorHandler(NOFILE,file);
         return NULL;
        }
      }
    }
   if (!(fgets(temp,MAXSTRING,in))) 
   ErrorHandler(ERROR_READING,"malformated header in PGM/PPM file");
   
   type=GRAYIMG;
   if (strncmp(temp,PGMMAGIC,2)) 
    {
     if (strncmp(temp,PPMMAGIC,2))
     ErrorHandler(UNKNOWN_GFXFORMAT,"is this a PGM/PPM file?");
     type=RGBIMG;
    }
   
   do
    {
     if (!fgets(temp,MAXSTRING,in)) 
     ErrorHandler(ERROR_READING,"malformated header in PGM/PPM file");
    }
   while(!strncmp(temp,"#",1));
   
   i=0;
   while(strncmp(&(temp[i++])," ",1));
   
   xmax=atoi(temp);
   ymax=atoi(&(temp[i]));
   
   if (!fgets(temp,MAXSTRING,in)) 
   ErrorHandler(ERROR_READING,"malformated header in PGM/PPM file");
   
   if (type==GRAYIMG) vprintf(stderr,"\n   PGM file: '%s'",filename);
   if (type==RGBIMG)  vprintf(stderr,"\n   PPM file: '%s'",filename);
   
   vprintf(stderr,"\n   Size:     (%d,%d)",xmax,ymax);
   
   if (type==GRAYIMG) /* pgm load */
    {
     TmpImg=GimmeABitMap(xmax,ymax,GRAYIMG);  /* allocates memory for SourceImage */
     for(y=0;y<ymax;y++)
      {
       if (!fread(bitarray,xmax,1,in))
       ErrorHandler(ERROR_READING,"malformated body in PGM file");
       for(x=0;x<xmax;x++) TmpImg->Map[x][y] = bitarray[x];
      }
     
     if ((maxx) && (TmpImg->XSize>maxx)) TmpImg->XSize=maxx; /* test for maximum sizes */
     if ((maxy) && (TmpImg->YSize>maxy)) TmpImg->YSize=maxy;
     
     if(TmpImg->XSize%BlockSize) 
     TmpImg->XSize=TmpImg->XSize/BlockSize*BlockSize; /* chop blocksize */
     if(TmpImg->YSize%BlockSize) 
     TmpImg->YSize=TmpImg->YSize/BlockSize*BlockSize;
     
     if((offx+TmpImg->XSize)>xmax) offx=xmax-TmpImg->XSize;
     if((offy+TmpImg->YSize)>ymax) offy=ymax-TmpImg->YSize;
     
     SourceImage=GimmeABitMap(TmpImg->XSize,TmpImg->YSize,GRAYIMG);
     Map2BitMap(TmpImg,SourceImage,offx,offy);
     
     vprintf(stderr,"\n   Truncated:(%d,%d)",TmpImg->XSize,TmpImg->YSize);
     vprintf(stderr,"\n   Offset:   [%d,%d]",offx,offy);
     
     FreeMeABitMap(TmpImg);
    }
   else if (type==RGBIMG) /* ppm load */
    {
     TmpImgR=GimmeABitMap(xmax,ymax,RGBIMG);  /* allocates memory for SourceImage */
     TmpImgG=GimmeABitMap(xmax,ymax,RGBIMG);
     TmpImgB=GimmeABitMap(xmax,ymax,RGBIMG);
     
     for(y=0;y<ymax;y++) 
      {
       if (!fread(bitarray,xmax+xmax+xmax,1,in)) 
       ErrorHandler(ERROR_READING,"malformated body in PGM file");
       for(x=0;x<xmax;x++)  
        {
         register unsigned int dum=3*x;
         TmpImgR->Map[x][y]=bitarray[dum];
         TmpImgG->Map[x][y]=bitarray[dum+1];
         TmpImgB->Map[x][y]=bitarray[dum+2];
        }
      }
     
     if ((maxx) && (TmpImgR->XSize>maxx)) 
      {
       TmpImgR->XSize=maxx;
       TmpImgG->XSize=maxx;
       TmpImgB->XSize=maxx;
      }
     
     if ((maxy) && (TmpImgR->YSize>maxy))
      {
       TmpImgR->YSize=maxy;
       TmpImgG->YSize=maxy;
       TmpImgB->YSize=maxy;
      }
     
     if(TmpImgR->XSize%BlockSize) 
      {
       TmpImgR->XSize=TmpImgR->XSize/BlockSize*BlockSize;
       TmpImgG->XSize=TmpImgG->XSize/BlockSize*BlockSize;
       TmpImgB->XSize=TmpImgB->XSize/BlockSize*BlockSize;
      }
     
     if(TmpImgR->YSize%BlockSize)
      {
       TmpImgR->YSize=TmpImgR->YSize/BlockSize*BlockSize;
       TmpImgG->YSize=TmpImgG->YSize/BlockSize*BlockSize;
       TmpImgB->YSize=TmpImgB->YSize/BlockSize*BlockSize;
      }
     
     if((offx+TmpImgR->XSize)>xmax)
      {
       /* offx=xmax-TmpImgR->XSize;
       offx=xmax-TmpImgG->XSize; */
       offx=xmax-TmpImgB->XSize;
      }
     
     if((offy+TmpImgR->YSize)>ymax)
      {
       /* offy=ymax-TmpImgR->YSize;
       offy=ymax-TmpImgG->YSize; */
       offy=ymax-TmpImgB->YSize;
      }
     
     SourceImage=GimmeABitMap(TmpImgR->XSize,3*TmpImgR->YSize,RGBIMG);
     TmpImg=GimmeABitMap(TmpImgR->XSize,TmpImgR->YSize,RGBIMG);
     
     Map2BitMap(TmpImgR,SourceImage,offx,offy);
     
     Map2BitMap(TmpImgG,TmpImg,offx,offy);
     BitMap2Map(TmpImg,SourceImage,0,TmpImgR->YSize);
     Map2BitMap(TmpImgB,TmpImg,offx,offy);
     BitMap2Map(TmpImg,SourceImage,0,TmpImgR->YSize*2);
     
     vprintf(stderr,"\n   Truncated:(%d,%d)",TmpImg->XSize,TmpImg->YSize);
     vprintf(stderr,"\n   Offset:   [%d,%d]",offx,offy);
     
     FreeMeABitMap(TmpImg);
     FreeMeABitMap(TmpImgR);
     FreeMeABitMap(TmpImgG);
     FreeMeABitMap(TmpImgB);
    }
   else ErrorHandler(UNKNOWN_GFXFORMAT,"Currently no supporting of this format");
   
   SourceImage->ImgType=type;
   
   if (fclose(in)) ErrorHandler(UNABLE_TO_CLOSE,"Unable to close file");
   
   return SourceImage;
  }
 
/**************************************|****************************************
 "Routine   : SaveBitMap"
Input  par: char *filename  (the name of the file to be used)
            Bitmap *DecodeImage (pointer to the decoded image to be saved)
Output par: none
Global par: none
Function  : Save the decoded image in the file filename.
***************************************|***************************************/
 
 void SaveBitMap(char *file,BitMap *Image,char *path)
  {
   char filename[MAXFILE],bitarray[MAXARRAY];
   unsigned int x,y,ysize;
   char magic[3];
   FILE *out;
   
   strcpy(filename,path);
   strcat(filename,file);
   
   if (Image->ImgType==GRAYIMG)
    {
     strcat(filename,PGMSFX);
     strcpy(magic,PGMMAGIC);
     ysize=Image->YSize;
    }
   else if (Image->ImgType==RGBIMG) 
    {
     strcat(filename,PPMSFX);
     strcpy(magic,PPMMAGIC);
     ysize=Image->YSize/3;
    }
   else ErrorHandler(UNKNOWN_GFXFORMAT,"Not supporting saving of that format");
   
   if (!(out=fopen(filename,"wb"))) 
   ErrorHandler(UNABLE_TO_OPEN,filename);
   fprintf(out,"%s\n# Creator: Limbo version %s\n%d %d\n%d\n",
           magic,LIMBOVERSION,Image->XSize,ysize,255);
   
   if (Image->ImgType==GRAYIMG)
   for(y=0;y<(Image->YSize);y++)
    {
     for(x=0;x<(Image->XSize);x++) bitarray[x] = Image->Map[x][y];
     if (!fwrite(bitarray,Image->XSize,1,out)) 
     ErrorHandler(ERROR_WRITING,"could not write body in PGM file");
    }
   else if (Image->ImgType==RGBIMG)
   for(y=0;y<ysize;y++)
    {
     for(x=0;x<(Image->XSize);x++)
      {
       register unsigned int dum=x*3;
       bitarray[dum]   = Image->Map[x][y];
       bitarray[dum+1] = Image->Map[x][y+ysize];
       bitarray[dum+2] = Image->Map[x][y+ysize*2];
      }
     if (!fwrite(bitarray,Image->XSize*3,1,out))  
     ErrorHandler(ERROR_WRITING,"could not write body in PGM file");
    }
   
   if(fclose(out)) ErrorHandler(UNABLE_TO_CLOSE,"Unable to close PGM file");
  }
 
 
 BitMap3D *LoadBitMap3D(char *file,int maxx,int maxy,int maxz,int offx,
                        int offy,int BlockSize,char *path)
  {
   BitMap3D *SourceImage;
   BitMap  *frame[999];
   char filename[MAXFILE],bitmapsfx[5];
   FILE *in;
   register unsigned int frames=0,i,z;
   char sfx[4];
   int quiet=Quiet;
   
   vprintf(stderr,"\nLoading Sequence...\n   Frames: ");
   
   strcpy(bitmapsfx,"");
   
   strcpy(filename,path);
   strcat(filename,file);
   strcat(filename,".000");
   
   if (!(in=fopen(filename,"rb"))) /* no suffix */
    {
     strcpy(filename,path);
     strcat(filename,file);
     strcat(filename,".000.pgm");
     strcpy(bitmapsfx,".pgm");
     if (!(in=fopen(filename,"rb"))) /* no suffix */
      {
       strcpy(filename,path);
       strcat(filename,file);
       strcat(filename,".000.ppm");
       strcpy(bitmapsfx,".ppm");
       if (!(in=fopen(filename,"rb"))) /* no suffix */ ErrorHandler(UNABLE_TO_OPEN,file);
      }
    }
   
   do
    {
     fclose(in);
     frames++;
     
     strcpy(filename,path);
     strcat(filename,file);
     strcat(filename,".");
     
     sfx[3]='\0';
     sfx[2]=48+(frames-(frames/10)*10);
     sfx[1]=48+(frames/10-(frames/100)*10);
     sfx[0]=48+(frames/100-(frames/1000)*10);
     strncat(filename,sfx,3);
     strcat(filename,bitmapsfx);
     
    }
   while((in=fopen(filename,"rb")) && (frames<maxz || maxz==0));
   
   for(i=0;i<frames;i++)
    {
     Quiet=quiet;
     vprintf(stderr,"[%d]",i);
     
     strcpy(filename,file);
     strcat(filename,".");
     
     sfx[3]='\0';
     sfx[2]=48+(i-(i/10)*10);
     sfx[1]=48+(i/10-(i/100)*10);
     sfx[0]=48+(i/100-(i/1000)*10);
     strncat(filename,sfx,3);
     strcat(filename,bitmapsfx);
     
     Quiet=TRUE;
     frame[i]=LoadBitMap(filename,maxx,maxy,offx,offy,BlockSize,path);
     if (frame[i]->XSize!=frame[0]->XSize ||
         frame[i]->YSize!=frame[0]->YSize ||
         frame[i]->ImgType!=frame[0]->ImgType) 
     ErrorHandler(WRONG_FORMAT,"frames not in the same format");
    }
   Quiet=quiet; /* restore quiet flat */
   
   vprintf(stderr,"\n   Found %d frames of size %d %d",frames,frame[0]->XSize,frame[0]->YSize);
   z=frames;
   if(z%BlockSize) z=z/BlockSize*BlockSize;
   if (z<BlockSize) ErrorHandler(OUT_OF_RANGE,"Too few frames in sequence");
   vprintf(stderr,"\n   Truncated to %d frames of size %d %d",z,frame[0]->XSize,frame[0]->YSize);
   
   SourceImage=GimmeABitMap3D(frame[0]->XSize,frame[0]->YSize,z,frame[0]->ImgType);
   
   for(i=0;i<z;i++) BitMap2Frame(frame[i],SourceImage,i);
   for(i=0;i<frames;i++) FreeMeABitMap(frame[i]);
   
   return SourceImage;
  }
 
 void SaveBitMap3D(char *file,BitMap3D *Seq,char *path)
  {
   BitMap *frame=GimmeABitMap(Seq->XSize,Seq->YSize,Seq->ImgType);
   char filename[MAXFILE];
   register unsigned int z;
   char sfx[4];
   
   vprintf(stderr,"\nSaving Sequence...\n   Frames: ");
   
   for(z=0;z<Seq->ZSize;z++)
    {
     Frame2BitMap(Seq,z,frame);
     
     strcpy(filename,file);
     strcat(filename,".");
     
     sfx[3]='\0';
     sfx[2]=48+(z-(z/10)*10);
     sfx[1]=48+(z/10-(z/100)*10);
     sfx[0]=48+(z/100-(z/1000)*10);
     strncat(filename,sfx,3);
     vprintf(stderr,"[%d]",z);
     
     SaveBitMap(filename,frame,path);
     
    }
   FreeMeABitMap(frame);
   
  }
