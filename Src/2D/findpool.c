#include "includes.h"
 
 BitMap *FindPool_SampleImg;
 
 
 int GridQuant(float Dist, int Size)
  {
   float s=(float)(Size)/2.0;
   int i=(int)(Dist+s-0.5);
   if (i>=Size) i=Size-1;
   if (i<0)     i=0;
      
   return i;
   
  }
 
/**************************************|****************************************
Routine   :FindNodes3D
Input  par:BitMap3D *Image 
           PoolStructure *Pool,int B,int delta,
                          int FeatureDim,float fTShadeEdge, int domain
Output par:
Function  :
***************************************|***************************************/
 
 PoolNode ***FindNodes(BitMap *Image,PoolStructure *Pool,int B,int delta,
                       int FeatureDim,float fTShadeEdge, int domain)
  {
   PoolNode ***Array;
   PoolNode *Node;
   float *Features;
   int x,y,XMax,YMax,Count=0;
   
   if (domain) XMax=((Image->XSize<<1)-B)/delta +1;
   else        XMax=(Image->XSize)/B;
   if (domain) YMax=((Image->YSize<<1)-B)/delta +1;
   else        YMax=(Image->YSize)/B;
   
   Array=GimmeAPoolNodeArray(XMax,YMax);
   
   if (domain)
    {
     Pool->DomainX=XMax;
     Pool->DomainY=YMax;
    }
   else
    {
     Pool->RangeX=XMax;
     Pool->RangeY=YMax;
    }
   
   for(x=0;x<XMax;x++)
   for(y=0;y<YMax;y++)
    {
     Array[x][y]=Node=GimmeAPoolNode();
     
     if (domain) Features=Classify(Image,(x*delta)>>1,(y*delta)>>1,
                                   B>>1,FeatureDim,fTShadeEdge);
     else        Features=Classify(Image,x*delta,y*delta,
                                   B,FeatureDim,fTShadeEdge);
     
     Node->Features=Features;
     Node->Distance=NULL;
     
     if (Features[VAR]>fTShadeEdge)  
      {
       Node->x=x*delta;
       Node->y=y*delta;
       Count++;
      }
    }
   
   vprintf(stderr,"block size %3d, edges: %6d total: %6d  = %3.1f%c",B,Count,
           XMax*YMax,Count*100.0/(float)(XMax*YMax),'%');
   
   
   return Array;
  } 
 
 
 FeatureSpace *FindSpace(PoolStructure *Pool,int FeatureDim,int Size,float fTShadeEdge)  
  {
   FeatureSpace *S=GimmeAFeatureSpace(FeatureDim,Size);
   Grid         *Tmp;
   GridTerm     *T;
   PoolNode ***DPool=Pool->DomainNodes;
   PoolNode ***RPool=Pool->RangeNodes;
   PoolNode    *Temp;
   ListNode *TempNode;
   
   float *Count=GimmeAFloatArray(FeatureDim);
   float *Mean =GimmeAFloatArray(FeatureDim);
   float *Var  =GimmeAFloatArray(FeatureDim);
   float *InvDev=GimmeAFloatArray(FeatureDim);
   
   float temp;
   double t;
   register int x,y,i,itemp;
   
   for(i=0;i<FeatureDim;i++)   /* find mean for all (domain) features */
    {
     double mean=0.0,var=0.0;  /* find variance cov_ii for all features */
     Count[i]=0.0;
     for(x=0;x<Pool->DomainX;x++)
     for(y=0;y<Pool->DomainY;y++)
      {
       Temp=DPool[x][y];
       if (Temp!=NULL && Temp->Features[VAR]>fTShadeEdge)
        {
         t=(double)Temp->Features[i+PREDEFFEATURES];
         mean += t;
         var += t*t;
         Count[i]++;
        }
      }
     Mean[i]=(float)(mean/(double)Count[i]);
     t=(var-Count[i]*Mean[i]*Mean[i])/(Count[i]-1.0);
     Var[i]=(float)t;
     InvDev[i]=1.0/(float)sqrt(t);
    }
   
   for(x=0;x<Pool->DomainX;x++) /* insert domain blocks in feature grid */
   for(y=0;y<Pool->DomainY;y++) /* using Mahalanobis distance    */
    {
     Temp=DPool[x][y];
     if (Temp!=NULL && Temp->Features[VAR]>fTShadeEdge)
      {
       Temp->Distance=GimmeALongArray(FeatureDim);
       Temp->Index   =GimmeAIntArray(FeatureDim);
       Tmp=S->GridArray;
       for(i=0;i<FeatureDim;i++)
        {
         temp=(Temp->Features[i+PREDEFFEATURES]-Mean[i])*InvDev[i];
         Temp->Distance[i]=(long)(temp*F2LONG);
         itemp=GridQuant(temp,Size);
         Temp->Index[i] = itemp;
         Tmp=Tmp->Next[itemp];
        }
       
       T=(GridTerm *)Tmp;
       if (T->Class==NULL)
        {
         T->Class=GimmeAListNode();
         TempNode=T->Class;
        }
       else
        {
         TempNode=T->Class;
         while (TempNode->Next!=NULL) TempNode=TempNode->Next;
         
         TempNode->Next=GimmeAListNode();
         TempNode=TempNode->Next;
        }
       
       TempNode->Domain=Temp;
       T->Count++;
      }
    }
   
   for(x=0;x<Pool->RangeX;x++) /* insert range blocks in feature grid */
   for(y=0;y<Pool->RangeY;y++) /* using Mahalanobis distance    */
    {
     Temp=RPool[x][y];
     if (Temp->Features[VAR]>fTShadeEdge)
      {
       Temp->Distance=GimmeALongArray(FeatureDim);
       Temp->Index   =GimmeAIntArray(FeatureDim);
       for(i=0;i<FeatureDim;i++)
        {
         temp=(Temp->Features[i+PREDEFFEATURES]-Mean[i])*InvDev[i];
         Temp->Distance[i]=(long)(temp*F2LONG);
         itemp=GridQuant(temp,Size);
         Temp->Index[i]=itemp;
        }
      }
    }
   
   FreeMeAFloatArray(Count);
   FreeMeAFloatArray(Mean);
   FreeMeAFloatArray(Var);
   FreeMeAFloatArray(InvDev);
   
   return S;
  }
 
 
/**************************************|****************************************
Routine   : *FindPool
Input  par: BitMap *Image, int D, int B, int delta, int keepdelta
Output par: none
Function  : Finds the pool structure, calls FindNodes recursively finding domain
            and range pool for every resolution 
***************************************|***************************************/
 
 PoolStructure *FindPool(BitMap *Image,int FeatureDimensions, int GridRes,
                         int TShadeEdge, int NSquare,int B,int delta)
  {
   if (NSquare>=0)
    {
     PoolStructure *Pool=GimmeAPoolStructure();
     register unsigned int x,y,i,j;
     register int Sum;
     static int first=TRUE;
     
     if(first) /* build a sampled version of the original image */
      {
       first=FALSE;
       FindPool_SampleImg=GimmeABitMap(Image->XSize>>1,Image->YSize>>1,
                                       Image->ImgType);
       for(x=0;x<Image->XSize;x+=2)      
       for(y=0;y<Image->YSize;y+=2)        
        {
         Sum=0;
         for(i=0;i<2;i++)          
         for(j=0;j<2;j++)  Sum+=Image->Map[x+i][y+j];
         FindPool_SampleImg->Map[x>>1][y>>1]=Sum>>2;
        }
      }
     
     Pool->SampledDomainBitmap=FindPool_SampleImg;
     
     vprintf(stderr,"\n   Range  ");
     Pool->RangeNodes=FindNodes(Image,Pool,B,B,FeatureDimensions,(float)TShadeEdge,FALSE);
     
     Pool->Next=FindPool(Image,FeatureDimensions,GridRes,
                         TShadeEdge,NSquare-1,B>>1,delta);
     
     
     vprintf(stderr,"\n   Domain ");
     Pool->DomainNodes=FindNodes(FindPool_SampleImg,Pool,B<<1,delta,
                                 FeatureDimensions,(float)TShadeEdge,TRUE);
     
     Pool->DomainSpace=FindSpace(Pool,FeatureDimensions,GridRes,(float)TShadeEdge);
     
     return Pool;
    }
   else return (PoolStructure *)NULL;
  }
 
