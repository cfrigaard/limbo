#include "includes.h"
 
 int Decode_Flag;
 BitMap3D *Decode_Block1;
 BitMap3D *Decode_Block2;
 BitMap3D *Decode_Image;
 
/**************************************|****************************************
Routine   :DecodeTrans3D
Input  par:Transformation *Trans (pointer to transformation to be decoded)
           int XPos,int YPos,int ZPos (xy and z position in image of trans)
           unsigned int LLevel (expansion level)
Output par:none
Function  :Takes a singel transformation and decodes it to an image block.
***************************************|***************************************/
 
 void DecodeTrans3D(Transformation *Trans,int XPos,int YPos,int ZPos,unsigned int LLevel)
  {
   Decode_Block1->XSize=Decode_Block1->YSize=Decode_Block1->ZSize=Trans->BlockSize<<LLevel;
   Decode_Block2->XSize=Decode_Block2->YSize=Decode_Block2->ZSize=Trans->BlockSize<<LLevel;
   
   switch(Trans->Type)
    {
     case EDGEBLOCK:
     
     Sample3D(Decode_Image,Decode_Block1,(Trans->Domain->x)<<LLevel,
              (Trans->Domain->y)<<LLevel,(Trans->Domain->z)<<LLevel);
     T_CScaleAndLShift3D(Decode_Block1,Trans->Alpha,Trans->Deltag);
     Isometri3D(Decode_Block1,Decode_Block2,Trans->tn+1);
     BitMap3D2Map3D(Decode_Block2,Decode_Image,(XPos<<LLevel),
                    (YPos<<LLevel),(ZPos<<LLevel));
     break;
     case SHADEBLOCK:
     if (Decode_Flag)
      {
       T_AbsorbGrey3D(Decode_Block1,(unsigned char)Trans->g0);
       BitMap3D2Map3D(Decode_Block1,Decode_Image,(XPos<<LLevel),
                      (YPos<<LLevel),(ZPos<<LLevel));
      }
     break;
     case NOBLOCK:
     break;
     default: ErrorHandler(OUT_OF_RANGE,"Unknown block type in Decode");
     break;
    }
   
   if(Trans->Sub)
    {
     register unsigned int i,j,k;
     for(i=0;i<2;i++)
     for(j=0;j<2;j++)
     for(k=0;k<2;k++)
     if(Trans->Sub[i][j][k])
     DecodeTrans3D(Trans->Sub[i][j][k],XPos+(i*(Trans->Sub[i][j][k]->BlockSize))
                   ,YPos+(j*(Trans->Sub[i][j][k]->BlockSize)),
                   ZPos+(k*(Trans->Sub[i][j][k]->BlockSize)),LLevel);
    }
  }
 
/**************************************|****************************************
Routine   :Decode3D
Input  par:Transformation ****FCCodes (FCCodes to be decoded)
           int ImgType (image type)
           int xsize, int ysize, int zsize (image size)
           int StartBlockSize (maximum block size)
           int Iterations (iterateions)
           int LLevels (expand at llevels)
Output par:*BitMap3D (pointer to a 3d bitmap)
Function  :Decodes an array of fccodes to a bitmap structure.
***************************************|***************************************/
 
 BitMap3D *Decode3D(Transformation ****FCCodes,int ImgType, int xsize, int ysize,
                    int zsize,int StartBlockSize, int Iterations, int LLevels)
  {
   register int i,j,k,l;
   
   vprintf(stderr,"\nDecoding...");
   
   Decode_Flag=TRUE;
   Decode_Block1=GimmeABitMap3D(StartBlockSize,StartBlockSize,StartBlockSize,ImgType);
   Decode_Block2=GimmeABitMap3D(StartBlockSize,StartBlockSize,StartBlockSize,ImgType);
   Decode_Image=GimmeABitMap3D(xsize,ysize,zsize,ImgType);
   
   for (l=0;l<Iterations;l++) /* decode iterations */
    {
     for (i=0;i<xsize/StartBlockSize;i++)
     for (j=0;j<ysize/StartBlockSize;j++)
     for (k=0;k<zsize/StartBlockSize;k++)
     DecodeTrans3D(FCCodes[i][j][k],i*StartBlockSize,j*StartBlockSize,k*StartBlockSize,0);

     Decode_Flag=FALSE;
     if (Quiet) fprintf(stderr,".");
     else fprintf(stderr,"\n   (%d/%d)",l+1,Iterations);
    }
   
   if (LLevels) /* expansion iterations */
    {
     Decode_Block1->XSize=Decode_Block1->YSize=Decode_Block1->ZSize=StartBlockSize;
     Decode_Block2->XSize=Decode_Block2->YSize=Decode_Block2->ZSize=StartBlockSize;
     
     for (l=1;l<LLevels+1;l++) 
      {
       Decode_Block1=Expand23D(Decode_Block1);
       Decode_Block2=Expand23D(Decode_Block2);
       Decode_Image=Expand23D(Decode_Image);
       for (i=0;i<xsize/StartBlockSize;i++)
        {
         for (j=0;j<ysize/StartBlockSize;j++)
         for (k=0;k<zsize/StartBlockSize;k++)
         DecodeTrans3D(FCCodes[i][j][k],i*StartBlockSize,
                       j*StartBlockSize,k*StartBlockSize,l);

        }
       if (Quiet) fprintf(stderr,",");
       else fprintf(stderr,"\n   [%d/%d]",l,LLevels);
      }
    }
  
   Decode_Block1->XSize=Decode_Block1->YSize=Decode_Block1->ZSize=StartBlockSize;
   Decode_Block2->XSize=Decode_Block2->YSize=Decode_Block2->ZSize=StartBlockSize;
   FreeMeABitMap3D(Decode_Block1);
   FreeMeABitMap3D(Decode_Block2);
   
   return Decode_Image;
  }
 
