 unsigned long GetClassCount(FeatureSpace *S,int *Index)
  ;
 ListNode *FindNearest(ListNode *List,PoolNode *Range,int dim, int MaxList)
  ;
 ListNode *BuildList(FeatureSpace *S,int *Index)
  ;
 ListNode *InsertList(ListNode *List,ListNode *Xtra)
  ;
 ListNode *XtraSearch(FeatureSpace *S,ListNode *List,int *Index,int shell,int dim,int end)
  ;
 ListNode *SearchGrid(FeatureSpace *DomainS,PoolNode *RangeNode,int MinList,int MaxList)
  ;
 Transformation *FindTransformation(int XPos,int YPos, int NSquare,
                                      int BlockSize,PoolStructure *Pool,
                                      int MinList,int MaxList)
  ;
 Transformation ***Encode(BitMap *Image,int NSquare,int StartBlockSize,
                             PoolStructure *Pool,Parameter *Pa,int MinList,int MaxList)
  ;
