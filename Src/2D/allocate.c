#include "includes.h"
 
/**************************************|****************************************
Routine   : GimmeAPoolNode
Input  par: none
Output par: PoolNode * (pointer to PoolNode)
Function  : Allocates a PoolNode structure. Initializes Next in 
            structure PoolNode to zero.
***************************************|***************************************/
 
 PoolNode *GimmeAPoolNode(void)
  {
   PoolNode *p=(PoolNode *)malloc(sizeof(PoolNode));
   if (p==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeAPoolNode not able to allocate structure");
   p->x=p->y=p->z=0;
   p->Features=NULL; /* initialize */
   p->Distance=NULL;
   p->Index=NULL;
   p->Next=NULL;
   return p;
  }
 
/**************************************|****************************************
Routine   : FreeMeAPoolNode"
Input  par: PoolNode *El (pointer to PoolNode) 
Output par: none
Function  : Frees a PoolNode structure.
***************************************|***************************************/
 
 void FreeMeAPoolNode(PoolNode *p)
  {
   if (p==NULL) ErrorHandler(NULL_POINTER,"FreeMeAPoolNode with null pointer");
   free(p);
  }
 
/**************************************|****************************************
Routine   : GimmeAPoolStructure"
Input  par: none
Output par: PoolStructure * (pointer to PoolStructure)
Function  : Allocates a PoolStructure. Initializes pointers to Edges,
            Shadesto null and B, D = 0.
***************************************|***************************************/
 
 PoolStructure *GimmeAPoolStructure(void)
  {
   PoolStructure *p=(PoolStructure *)malloc(sizeof(PoolStructure));
   if (p==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeAPoolStructure not able to allocate structure");
   p->DomainNodes=p->RangeNodes=NULL;
   p->Next=NULL;
   p->D=p->B=0;
   return p;
  }
 
/**************************************|****************************************
Routine   : FreeMeAPoolStructure"
Input  par: PoolStructure *p (pointer to PoolStructure) 
Output par: none
Function  : Frees a PoolStructure.
***************************************|***************************************/
 
 void FreeMeAPoolStructure(PoolStructure *p)
  {
   if (p==NULL) ErrorHandler(NULL_POINTER,"FreeMeAPoolStructure with null pointer");
   free(p);
  }
 
/**************************************|****************************************
Routine   : GimmeAPoolNodeArray"
Input  par: int xsize (x size of PoolNodearray)
            int ysize (y size of PoolNodearray)
Output par: PoolNode ***  (pointer to 2D array of PoolNode-pointers)
Function  : Allocates a 2D structure of Poolnodes. No Initializing.
***************************************|***************************************/
 
 PoolNode ***GimmeAPoolNodeArray(int xsize,int ysize)
  {
   int i;
   PoolNode ***Array;
   
   Array=(PoolNode ***) calloc(xsize,sizeof(PoolNode **));
   if (Array==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeAPoolNodeArray3D not able to allocate struture");
   for (i=0;i<xsize;i++)
    {
     Array[i]=(PoolNode **) calloc(ysize,sizeof(PoolNode *));
     if (Array[i]==NULL) 
     ErrorHandler(NO_FREE_MEMORY,"GimmeAPoolNodeArray3D not able to allocate structure");
    }
   return Array;
  }
 
/**************************************|****************************************
Routine   : FreeMeAPoolNodeArray"
Input  par: PoolArray *p (pointer to PoolNodeArray)
            int xsize    (xsize of array) 
Output par: none
Function  : Frees a PoolNodeArray Node.
***************************************|***************************************/
 
 void FreeMeAPoolNodeArray(PoolNode ****p,int xsize)
  {
   register unsigned int i;
   
   if (p==NULL) ErrorHandler(NULL_POINTER,"FreeMeAPoolNodeArray3D with null pointer");
   for (i=0;i<xsize;i++) 
    {
     free(p[i]);
    }
   free(p);
  }
 
/**************************************|****************************************
Routine   : GimmeATransformation"
Input  par: none
Output par: Transformation * (pointer to Transformation)
Function  : Allocates a Transformation structure. Initializes Domain and Sub.
***************************************|***************************************/
 
 Transformation *GimmeATransformation(void)
  {
   Transformation *p=(Transformation *)malloc(sizeof(Transformation));
   if (p==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeATransformation not able to allocate structure");
   p->Domain=NULL; /* initialize */
   p->Sub=NULL;
   return p;
  }
 
/**************************************|****************************************
Routine   : FreeMeATransformation"
Input  par: Transformation *Tr (pointer to Transformation) 
Output par: none
Function  : Frees a Transformation structure.
***************************************|***************************************/
 
 void FreeMeATransformation(Transformation *Tr)
  {
   if (Tr==NULL) ErrorHandler(NULL_POINTER,"FreeMeATransformation with null pointer");
   free(Tr);
  }
 
/**************************************|****************************************
Routine   : GimmeATransArray"
Input  par: int xsize (x size of transarray)
            int ysize (y size of transarray)
Output par: Transformation *** (pointer to 2D array of transformation-pointers)
Function  : Allocates a 2D structure of transformationpointers. No Initializing.
***************************************|***************************************/
 
 
 Transformation ***GimmeATransArray(int xsize,int ysize)
  {
   int i;
   Transformation ***Array;
   
   Array=(Transformation ***) calloc(xsize,sizeof(Transformation **));
   if (Array==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeATransArray3D not able to allocate struture");
   for (i=0;i<xsize;i++)
    {
     Array[i]=(Transformation **) calloc(ysize,sizeof(Transformation *));
     if (Array[i]==NULL) 
     ErrorHandler(NO_FREE_MEMORY,"GimmeATransArray3D not able to allocate structure");
    }
   return Array;
  }
 
/**************************************|****************************************
Routine   : FreeMeATransArray"
Input  par: TransArray *Tr (pointer to TransArray)
            int xsize      (xsize of array) 
Output par: none
Function  : Frees a TransArray structure.
***************************************|***************************************/
 
 void FreeMeATransArray3D(Transformation ****Tr,int xsize,int ysize)
  {
   register unsigned int i,j;
   
   if (Tr==NULL) ErrorHandler(NULL_POINTER,"FreeMeATransArray3D with null pointer");
   for (i=0;i<xsize;i++)
    {
     for (j=0;i<ysize;j++) free(Tr[i][j]);
     free(Tr[i]);
    }
   free(Tr);
  }
 
/**************************************|****************************************
Routine   : GimmeAParameter"
Input  par: none
Output par: Parameter * (pointer to Parameter)
Function  : Allocates a Parameter structure. No initializing.
***************************************|***************************************/
 
 Parameter *GimmeAParameter(void)
  {
   Parameter *p=(Parameter *)malloc(sizeof(Parameter));
   if (p==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeAParameter not able to allocate structure");
   return p;
  }
 
/**************************************|****************************************
Routine   : FreeMeAParameter"
Input  par: Parameter *p (pointer to Parameter) 
Output par: none
Function  : Frees a Parameter structure.
***************************************|***************************************/
 
 void FreeMeAParameter(Parameter *p)
  {
   if (p==NULL) ErrorHandler(NULL_POINTER,"FreeMeAParameter with null pointer");
   free(p);
  }
 
/**************************************|****************************************
Routine   : GimmeABitamap"
Input  par: int xsize (x size of bitmap)
            int ysize (y size of bitmap)
Output par: BitMap *  (pointer to BitMap)
Function  : Allocates a BitMap structure of size x,y. Initialize XSize=x 
            YSize=y  Map->Gfx array  ImgType=type
            Bitmap image is initialized to 0'oes.
***************************************|***************************************/
 
 BitMap *GimmeABitMap(int xsize, int ysize, int type)
  {
   int i;
   BitMap *Gfx;
   unsigned char **Array;
   
   Gfx=(BitMap *) malloc(sizeof(BitMap));
   if (Gfx==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeABitMap not able to allocate structure");
   
   Array=(unsigned char **) calloc(xsize,sizeof(unsigned char *));
   if (Array==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeABitMap not able to allocate structure");
   for (i=0;i<xsize;i++)
    {
     Array[i]=(unsigned char *) calloc(ysize,sizeof(unsigned char));
     if (Array[i]==NULL) 
     ErrorHandler(NO_FREE_MEMORY,"GimmeABitMap not able to allocate structure");
    }
   Gfx->Map=Array;  /* initialize */
   Gfx->XSize=xsize;
   Gfx->YSize=ysize;
   Gfx->ImgType=type;
   
   return Gfx;
  }
 
/**************************************|****************************************
Routine   : FreeMeABitMap"
Input  par: BitMap *Bm (pointer to BitMap) 
Output par: none
Function  : Frees a BitMap structure.
***************************************|***************************************/
 
 void FreeMeABitMap(BitMap *Bm)
  {
   register int i;
   if (Bm==NULL) ErrorHandler(NULL_POINTER,"FreeMeABitMap with null pointer");
   
   for(i=0;i<Bm->XSize;i++) free(Bm->Map[i]);
   free(Bm->Map);
   free(Bm);
  }
 
 
 ListNode *GimmeAListNode(void)
  {
   ListNode *p=(ListNode *)malloc(sizeof(ListNode));
   if (p==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeAListNode not able to allocate structure");
   p->Info=0;
   p->Domain=NULL; /* initialize */
   p->Next=NULL; /* initialize */
   p->Pred=NULL;
   
   return p;
  }
 
 
 BitMap3D *GimmeABitMap3D(int xsize, int ysize, int zsize, int type)
  {
   register int i,j;
   BitMap3D *Gfx;
   unsigned char ***Array;
   
   Gfx=(BitMap3D *) malloc(sizeof(BitMap3D));
   if (Gfx==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeABitMap3D not able to allocate structure");
   
   Array=(unsigned char ***) calloc(xsize,sizeof(unsigned char **));
   if (Array==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeABitMap3D not able to allocate structure");
   for (i=0;i<xsize;i++)
    {
     Array[i]=(unsigned char **) calloc(ysize,sizeof(unsigned char*));
     if (Array[i]==NULL) 
     ErrorHandler(NO_FREE_MEMORY,"GimmeABitMap3D not able to allocate structure");
     for (j=0;j<ysize;j++)
      {
       Array[i][j]=(unsigned char *) calloc(zsize,sizeof(unsigned char));
       if (Array[i][j]==NULL) 
       ErrorHandler(NO_FREE_MEMORY,"GimmeABitMap3D not able to allocate structure");
      }
    }
   Gfx->Map=Array;   /* initialize */
   Gfx->XSize=xsize;
   Gfx->YSize=ysize;
   Gfx->ZSize=zsize;
   Gfx->ImgType=type;
   
   return Gfx;
  }
 
/**************************************|****************************************
Routine   : FreeMeABitMap3D"
Input  par: BitMap3D *Bm (pointer to BitMap3D) 
Output par: none
Function  : Frees a BitMap3D structure.
***************************************|***************************************/
 
 void FreeMeABitMap3D(BitMap3D *Bm)
  {
   register int i,j;
   if (Bm==NULL) ErrorHandler(NULL_POINTER,"FreeMeABitMap3D with null pointer");
   
   for(i=0;i<Bm->XSize;i++) 
    {
     for(j=0;j<Bm->YSize;j++) free(Bm->Map[i][j]);
     free(Bm->Map[i]);
    }
   free(Bm->Map);
   free(Bm);
  }
 
 
 void FreeMeAListNode(ListNode *El)
  {
   if (El==NULL) ErrorHandler(NULL_POINTER,"FreeMeAListNode with null pointer");
   free(El);
  }
 
 void FreeMeAList(ListNode *LP)
  {
   ListNode *tmp2,*tmp=LP,*Start=LP;
   if (LP==NULL) ErrorHandler(NULL_POINTER,"FreeMeAList with null pointer");
   do 
    {
     tmp2=tmp;
     tmp=tmp->Next;
     FreeMeAListNode(tmp2);
    }
   while((tmp!=NULL) && (tmp!=Start));
  }
 
 float *GimmeAFloatArray(int n) 
  {
   float *p=(float *)calloc(n,sizeof(float));
   if (p==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeAFloatArray not able to allocate structure");
   return p;
  }
 
 void FreeMeAFloatArray(float *Pa)
  {
   if (Pa==NULL) 
   ErrorHandler(NULL_POINTER,"FreeMeAFloatArray with null pointer");
   free(Pa);
  }
 
 long double *GimmeADoubleArray(int n) 
  {
   long double *p=(long double *)calloc(n,sizeof(long double));
   if (p==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeADoubleArray not able to allocate structure");
   return p;
  }
 
 void FreeMeADoubleArray(double *Pa)
  {
   if (Pa==NULL) 
   ErrorHandler(NULL_POINTER,"FreeMeADoubleArray with null pointer");
   free(Pa);
  }

 int *GimmeAIntArray(int n) 
  {
   int *p=(int *)calloc(n,sizeof(int));
   if (p==NULL) ErrorHandler(NO_FREE_MEMORY,"GimmeAIntArray not able to allocate structure");
   return p;
  }
 
 void FreeMeAIntArray(int *Pa)
  {
   if (Pa==NULL) 
   ErrorHandler(NULL_POINTER,"FreeMeAIntArray with null pointer");
   free(Pa);
  }
 
 unsigned int *GimmeAUIntArray(int n) 
  {
   unsigned int *p=(unsigned int *)calloc(n,sizeof(unsigned int));
   if (p==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeAUIntArray not able to allocate structure");
   return p;
  }
 
 void FreeMeAUIntArray(unsigned int *Pa)
  {
   if (Pa==NULL) 
   ErrorHandler(NULL_POINTER,"FreeMeAUIntArray with null pointer");
   free(Pa);
  }
 
 long *GimmeALongArray(int n) 
  {
   long *p=(long *)calloc(n,sizeof(long));
   if (p==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeALongArray not able to allocate structure");
   return p;
  }
 
 void FreeMeALongArray(long *Pa)
  {
   if (Pa==NULL) 
   ErrorHandler(NULL_POINTER,"FreeMeALongArray with null pointer");
   free(Pa);
  }
 
 unsigned long *GimmeAULongArray(int n) 
  {
   unsigned long *p=(unsigned long *)calloc(n,sizeof(unsigned long));
   if (p==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeAULongArray not able to allocate structure");
   return p;
  }
 
 void FreeMeAULongArray(unsigned long *Pa)
  {
   if (Pa==NULL) 
   ErrorHandler(NULL_POINTER,"FreeMeAULongArray with null pointer");
   free(Pa);
  }
 
 LimboHeader *GimmeALimboHeader(void) 
  {
   LimboHeader *p=(LimboHeader *)malloc(sizeof(LimboHeader));
   if (p==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeALimboHeader not able to allocate structure");
   return p;
  }
 
 void FreeMeALimboHeader(LimboHeader *head)
  {
   if (head==NULL) 
   ErrorHandler(NULL_POINTER,"FreeMeALimboHeader with null pointer");
   free(head);
  }
 
 
 FeatureSpace *GimmeAFeatureSpace(int dim,int size)
  {
   FeatureSpace *S;
   
   S=(FeatureSpace *)malloc(sizeof(FeatureSpace));
   if (S==NULL) 
   ErrorHandler(NO_FREE_MEMORY,"GimmeAFeatureSpace not able to allocate structure");
   S->Dimension=dim;
   S->Size=size;
   S->GridArray=GimmeAGrid(dim,size);
   
   return S;
  }
 
 void FreeMeAFeatureSpace(FeatureSpace *S)
  {
   if (S==NULL) ErrorHandler(NULL_POINTER,"FreeMeAFeatureSpace with null pointer");
   
   if (S->GridArray!=NULL) FreeMeAGrid(S->GridArray,S->Dimension,S->Size);
   free(S);
  }
 
 Grid *GimmeAGrid(int dim,int size)
  {
   Grid *G=NULL;
   GridTerm *GT;
   register int i;
   
   if (dim>0) 
    {
     G=(Grid *)malloc(sizeof(Grid));
     if (G==NULL) 
     ErrorHandler(NO_FREE_MEMORY,"GimmeAGrid not able to allocate structure");
     G->Next=(Grid **)calloc(size,sizeof(Grid *));
     if (G->Next==NULL)
     ErrorHandler(NO_FREE_MEMORY,"GimmeAGrid not able to allocate structure");
     for (i=0;i<size;i++) G->Next[i]=GimmeAGrid(dim-1,size);
    }
   
   if (dim==0)
    {
     GT=(GridTerm *)malloc(sizeof(GridTerm));
     if (GT==NULL)
     ErrorHandler(NO_FREE_MEMORY,"GimmeAGrid not able to allocate structure");
     GT->Class=NULL;
     GT->Count=0;
     G=(Grid *)GT;
    }
   
   return G;
  }
 
 void FreeMeAGrid(Grid *G,int Dim,int Size)
  {
   register int i;
   if (G==NULL)
   ErrorHandler(NULL_POINTER,"FreeMeAGrid with null pointer");
   
   if(Dim>0)
    {
     if (G->Next!=NULL) 
     for(i=0;i<Size;i++)
      {
       FreeMeAGrid(G->Next[i],Dim-1,Size);
       free(G->Next[i]);
      }
    }
   else 
    {
     if (G->Next!=NULL) 
     for(i=0;i<Size;i++) free((GridTerm *)G->Next[i]);
    }
   free(G);
  }
 
 
