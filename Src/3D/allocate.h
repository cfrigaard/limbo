 PoolNode *GimmeAPoolNode(void)
  ;
 void FreeMeAPoolNode(PoolNode *p)
  ;
 PoolStructure *GimmeAPoolStructure(void)
  ;
 void FreeMeAPoolStructure(PoolStructure *p)
  ;
 PoolNode ****GimmeAPoolNodeArray3D(int xsize,int ysize,int zsize)
  ;
 void FreeMeAPoolNodeArray3D(PoolNode ****p,int xsize,int ysize)
  ;
 Transformation *GimmeATransformation(void)
  ;
 void FreeMeATransformation(Transformation *Tr)
  ;
 Transformation ****GimmeATransArray3D(int xsize,int ysize,int zsize)
  ;
 void FreeMeATransArray3D(Transformation ****Tr,int xsize,int ysize)
  ;
 Parameter *GimmeAParameter(void)
  ;
 void FreeMeAParameter(Parameter *p)
  ;
 BitMap *GimmeABitMap(int xsize, int ysize, int type)
  ;
 void FreeMeABitMap(BitMap *Bm)
  ;
 ListNode *GimmeAListNode(void)
  ;
 BitMap3D *GimmeABitMap3D(int xsize, int ysize, int zsize, int type)
  ;
 void FreeMeABitMap3D(BitMap3D *Bm)
  ;
 void FreeMeAListNode(ListNode *El)
  ;
 void FreeMeAList(ListNode *LP)
  ;
 float *GimmeAFloatArray(int n) 
  ;
 void FreeMeAFloatArray(float *Pa)
  ;
 long double *GimmeADoubleArray(int n) 
  ;
 void FreeMeADoubleArray(double *Pa)
  ;
 int *GimmeAIntArray(int n) 
  ;
 void FreeMeAIntArray(int *Pa)
  ;
 unsigned int *GimmeAUIntArray(int n) 
  ;
 void FreeMeAUIntArray(unsigned int *Pa)
  ;
 long *GimmeALongArray(int n) 
  ;
 void FreeMeALongArray(long *Pa)
  ;
 unsigned long *GimmeAULongArray(int n) 
  ;
 void FreeMeAULongArray(unsigned long *Pa)
  ;
 LimboHeader *GimmeALimboHeader(void) 
  ;
 void FreeMeALimboHeader(LimboHeader *head)
  ;
 FeatureSpace *GimmeAFeatureSpace(int dim,int size)
  ;
 void FreeMeAFeatureSpace(FeatureSpace *S)
  ;
 Grid *GimmeAGrid(int dim,int size)
  ;
 void FreeMeAGrid(Grid *G,int Dim,int Size)
  ;
