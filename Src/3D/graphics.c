#include "includes.h"
 
 void Color(BitMap *map,unsigned char col)
  {
   register unsigned int x,y;
   for(x=0;x<map->XSize;x++)
   for(y=0;y<map->YSize;y++) map->Map[x][y]=col;
  }


 void Color3D(BitMap3D *map,unsigned char col)
  {
   register unsigned int x,y,z;
   for(x=0;x<map->XSize;x++)
   for(y=0;y<map->YSize;y++)
   for(z=0;z<map->ZSize;z++) map->Map[x][y][z]=col;
  }

 
 BitMap *Gen3D(BitMap3D *Org,int thres, int step,
               int framecol, int stepframecol,int background)
  {
   BitMap *Dst;
   int depth=Org->ZSize;
   int i,x,y,temp,d=(depth-1)*step;
   
   Dst=GimmeABitMap(Org->XSize+d,Org->YSize+d,Org->ImgType);
   Color(Dst,background);  
 
   for(i=depth;i>0;i--)
    {
     for(x=0;x<Org->XSize;x++)
     for(y=0;y<Org->YSize;y++)
      {
       temp=Org->Map[x][y][i-1];
       if (temp>thres) Dst->Map[x+(i-1)*step][y+(depth-i)*step]=temp;
      }
     if (stepframecol>=0) 
     Rect(Dst,(i-1)*step,(depth-i)*step,Org->XSize,Org->YSize,stepframecol);
    }  
 
   if (framecol>=0)
        {
     Rect(Dst,0,d,Org->XSize,Org->YSize,framecol);
     Line(Dst,0,d,d,0,framecol);
     Line(Dst,d,0,Org->XSize+d-1,0,framecol);
     Line(Dst,Org->XSize+d-1,0,Org->XSize,d-1,framecol);
     Line(Dst,Org->XSize+d-1,0,Org->XSize+d-1,Org->YSize,framecol);
     Line(Dst,Org->XSize-1,Org->YSize+d-1,Org->XSize+d-1,Org->YSize-1,framecol);
    }
   
   return Dst;
  }
 

 void Rect(BitMap *map,int x, int y, int dx, int dy, int col)
  {
   register int i;
   for (i=x;i<(x+dx);i++)
    {
     if (col<0) map->Map[i][y]=Invers(map->Map[i][y]);
     else  map->Map[i][y]=col;
     if (col<0) map->Map[i][y+dy-1]=Invers(map->Map[i][y+dy-1]);
     else map->Map[i][y+dy-1]=col;
    }
   
   for (i=y+1;i<(y+dy)-1;i++)
    {
     if (col<0) map->Map[x][i]=Invers(map->Map[x][i]);
     else map->Map[x][i]=col;
     if (col<0) map->Map[x+dx-1][i]=Invers(map->Map[x+dx-1][i]);
     else map->Map[x+dx-1][i]=col;
    }
  }
 
 void Line(BitMap *map, int x1, int y1, int x2, int y2, int col)
  {
   register unsigned int i;
   int temp;
   float alpha;
   
   if (x2-x1!=0) 
    {
     if (x1>x2) 
      {
       temp=x1;
       x1=x2;
       x2=temp;
       temp=y1;
       y1=y2;
       y2=temp;
      }
     alpha=(float)(y2-y1)/(float)(x2-x1);
     
     if ((alpha<=1.0) && (alpha>=-1.0))
      {
       for (i=x1;i<=x2;i++) 
       if (col<0) map->Map[i][(int)(alpha*(i-x1))+y1]=
       Invers(map->Map[i][(int)(alpha*(i-x1))+y1]);
       else map->Map[i][(int)(alpha*(i-x1))+y1]=col;
      }
     else 
      {
       alpha=1.0/alpha;
       if (y1>y2) 
        {
         temp=x1;
         x1=x2;
         x2=temp;
         temp=y1;
         y1=y2;
         y2=temp;
        }
       for (i=y1;i<=y2;i++) 
       if (col<0) map->Map[(int)(alpha*(i-y1))+x1][i]=
       Invers(map->Map[(int)(alpha*(i-y1))+x1][i]);
       else map->Map[(int)(alpha*(i-y1))+x1][i]=col;
      }
    }
   else 
    {
     if (y1>y2) 
      {
       temp=y1;
       y1=y2;
       y2=temp;
      }
     for (i=y1;i<=y2;i++)
     if (col<0) map->Map[x1][i]=Invers(map->Map[x1][i]);
     else map->Map[x1][i]=col;
    }
  }
