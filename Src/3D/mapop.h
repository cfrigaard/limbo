 void Map2BitMap(BitMap *SrcImg,BitMap *DstImg,int XStart,int YStart)
  ;
 void BitMap2Map(BitMap *SrcImg,BitMap *DstImg,int XStart,int YStart)
  ;
 void Map3D2BitMap3D(BitMap3D *SrcImg,BitMap3D *DstImg,int XStart,int YStart,int ZStart)
  ;
 void BitMap3D2Map3D(BitMap3D *SrcImg,BitMap3D *DstImg,int XStart,int YStart,int ZStart)
  ;
 void CopyBitMap3D(BitMap3D *SrcImg,BitMap3D *DstImg)
  ;
 void Frame2BitMap(BitMap3D *SrcImg,int z,BitMap *DstImg)
  ;
 void BitMap2Frame(BitMap *SrcImg,BitMap3D *DstImg,int z)
  ;
 void T_CScaleAndLShift3D(BitMap3D *SrcImg,float Scale,int Shift)
  ;
 void T_AbsorbGrey3D(BitMap3D *SrcImg, unsigned char Par)
  ;
 void Sample3D(BitMap3D *SrcImg,BitMap3D *DstImg,int XPos,int YPos,int ZPos)
  ;
 BitMap3D *Expand23D(BitMap3D *SrcImg)
  ;
 float PSNR3D(BitMap3D *ImgA,BitMap3D *ImgB)
  ;
 unsigned long dl23D(BitMap3D *ImgA,int AXPos,int AYPos, int AZPos,BitMap3D *ImgB)
  ;
 void Isometri3D(BitMap3D *SrcImg,BitMap3D *DstImg, int type)
  ;
 unsigned long IsoAnddl23D(BitMap3D *ImgA, int AXPos, int AYPos, int AZPos,
                           BitMap3D *ImgB, int Size, int type)
  ;
 unsigned char MakeRange(float p,Parameter *pa) /* asymetric quant */
  ;
 float MakeFloat(unsigned char conv,Parameter *pa)
  ;
