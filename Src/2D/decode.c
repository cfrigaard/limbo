#include "includes.h"
 
 int Decode_Flag;
 BitMap *Decode_Block1;
 BitMap *Decode_Block2;
 BitMap *Decode_Image;
 
/**************************************|****************************************
Routine   :DecodeTrans3D
Input  par:Transformation *Trans (pointer to transformation to be decoded)
           int XPos,int YPos,int ZPos (xy and z position in image of trans)
           unsigned int LLevel (expansion level)
Output par:none
Function  :Takes a singel transformation and decodes it to an image block.
***************************************|***************************************/
 
 void DecodeTrans(Transformation *Trans,int XPos,int YPos,unsigned int LLevel)
  {
   Decode_Block1->XSize=Decode_Block1->YSize=Trans->BlockSize<<LLevel;
   Decode_Block2->XSize=Decode_Block2->YSize=Trans->BlockSize<<LLevel;
   
   switch(Trans->Type)
    {
     case EDGEBLOCK:
     
     Sample(Decode_Image,Decode_Block1,(Trans->Domain->x)<<LLevel,
             (Trans->Domain->y)<<LLevel);
     T_CScaleAndLShift(Decode_Block1,Trans->Alpha,Trans->Deltag);
     Isometri(Decode_Block1,Decode_Block2,Trans->tn+1);
     BitMap2Map(Decode_Block2,Decode_Image,(XPos<<LLevel),
                    (YPos<<LLevel));
     break;
     case SHADEBLOCK:
     if (Decode_Flag)
      {
       T_AbsorbGrey(Decode_Block1,(unsigned char)Trans->g0);
       BitMap2Map(Decode_Block1,Decode_Image,(XPos<<LLevel),
                      (YPos<<LLevel));
      }
     break;
     case NOBLOCK:
     break;
     default: ErrorHandler(OUT_OF_RANGE,"Unknown block type in Decode");
     break;
    }
   
   if(Trans->Sub)
    {
     register unsigned int i,j;
     for(i=0;i<2;i++)
     for(j=0;j<2;j++)
     if(Trans->Sub[i][j])
     DecodeTrans(Trans->Sub[i][j],XPos+(i*(Trans->Sub[i][j]->BlockSize))
                   ,YPos+(j*(Trans->Sub[i][j]->BlockSize)),
                   LLevel);
    }
  }
 
/**************************************|****************************************
Routine   :Decode3D
Input  par:Transformation ****FCCodes (FCCodes to be decoded)
           int ImgType (image type)
           int xsize, int ysize, int zsize (image size)
           int StartBlockSize (maximum block size)
           int Iterations (iterateions)
           int LLevels (expand at llevels)
Output par:*BitMap3D (pointer to a 3d bitmap)
Function  :Decodes an array of fccodes to a bitmap structure.
***************************************|***************************************/
 
 BitMap *Decode(Transformation ***FCCodes,int ImgType, int xsize, int ysize,
                  int StartBlockSize, int Iterations, int LLevels)
  {
   register int i,j,l;
   
   vprintf(stderr,"\nDecoding...");
   
   Decode_Flag=TRUE;
   Decode_Block1=GimmeABitMap(StartBlockSize,StartBlockSize,ImgType);
   Decode_Block2=GimmeABitMap(StartBlockSize,StartBlockSize,ImgType);
   Decode_Image=GimmeABitMap(xsize,ysize,ImgType); /* LoadBitMap("teach2",xsize,ysize,0,,1,ORGGFX);*/
   
   for (l=0;l<Iterations;l++) /* decode iterations */
    {
     for (i=0;i<xsize/StartBlockSize;i++)
     for (j=0;j<ysize/StartBlockSize;j++)
     DecodeTrans(FCCodes[i][j],i*StartBlockSize,j*StartBlockSize,0);

     Decode_Flag=FALSE;
     if (Quiet) fprintf(stderr,".");
     else fprintf(stderr,"\n   (%d/%d)",l+1,Iterations);
    }
   
   if (LLevels) /* expansion iterations */
    {
     Decode_Block1->XSize=Decode_Block1->YSize=StartBlockSize;
     Decode_Block2->XSize=Decode_Block2->YSize=StartBlockSize;
     
     for (l=1;l<LLevels+1;l++) 
      {
       Decode_Block1=Expand2(Decode_Block1);
       Decode_Block2=Expand2(Decode_Block2);
       Decode_Image=Expand2(Decode_Image);
       for (i=0;i<xsize/StartBlockSize;i++)
        {
         for (j=0;j<ysize/StartBlockSize;j++)
         DecodeTrans(FCCodes[i][j],i*StartBlockSize,
                       j*StartBlockSize,l);
         }
       if (Quiet) fprintf(stderr,",");
       else fprintf(stderr,"\n   [%d/%d]",l,LLevels);
      }
    }
  
   Decode_Block1->XSize=Decode_Block1->YSize=StartBlockSize;
   Decode_Block2->XSize=Decode_Block2->YSize=StartBlockSize;
   FreeMeABitMap(Decode_Block1);
   FreeMeABitMap(Decode_Block2);
   
   return Decode_Image;
  }
 
