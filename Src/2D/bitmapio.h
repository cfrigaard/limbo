 BitMap *LoadBitMap(char *file,int maxx,int maxy,int offx,int offy,
                    int BlockSize,char *path)
  ;
 void SaveBitMap(char *file,BitMap *Image,char *path)
  ;
 BitMap3D *LoadBitMap3D(char *file,int maxx,int maxy,int maxz,int offx,
                        int offy,int BlockSize,char *path)
  ;
 void SaveBitMap3D(char *file,BitMap3D *Seq,char *path)
  ;
