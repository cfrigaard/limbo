 void LearnUsage(void)
  ;
 void ErrorHandler(int error,char *message)
  ;
 void SaveAndView(char *fil, BitMap *map)
  ;
 void SaveAndView3D(char *fil, BitMap3D *map)
  ;
 void MyExit(int errno)
  ;
 int ReadInteger(char *convstring)
  ;
 float ReadFloat(char *convstring)
  ;
 char *GetEnv(char *environment)
  ;
 unsigned char Invers(unsigned char x)
  ;
