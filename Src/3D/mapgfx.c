#include "includes.h"
 
 int Quiet=FALSE;
 
 
 void MapUsage()
  {
   printf("\nMapGfx - Mapping operations for bitmaps");
   printf("\nUsage: mapgfx [-options] <command> <infile> <outfile>");
   printf("\n\nCommands:");
   printf("\n m     Map 2D image to 3D");
   printf("\n r     Remap 3D sequence to 2D");
   printf("\n 2     PSNR between 2D images");
   printf("\n 3     PSNR between 3D sequences");
   printf("\n p     Convert pgm to ppm");
   printf("\n\nOptions:\n");
   printf("\n -d<n>    Maximum 'x' size");
   printf("\n -b<n>    Maximum 'y' size");
   printf("\n -t<n>    Mapping type: 1 = Morton order");
   printf("\n                        2 = x subsample");
   printf("\n                        3 = y subsample");
   printf("\n                        4 = Peano-Hilbert order");
   printf("\n -q       Be quiet");
   
   MyExit(0);
  }
 
 BitMap3D *MortonOrder(BitMap3D *Img)
  {
   register unsigned int i,j,k,l;
   BitMap3D *Dst=GimmeABitMap3D(Img->XSize>>1,Img->YSize>>1,Img->ZSize<<2,Img->ImgType);
   static level=0;
   
   for(i=0;i<Dst->XSize;i++)
   for(j=0;j<Dst->YSize;j++)
    {
     for(k=0;k<Img->ZSize;k++) Dst->Map[i][j][0*Img->ZSize+k]=Img->Map[(i<<1)  ][(j<<1)  ][k];
     for(k=0;k<Img->ZSize;k++) Dst->Map[i][j][1*Img->ZSize+k]=Img->Map[(i<<1)+1][(j<<1)  ][k];
     for(k=0;k<Img->ZSize;k++) Dst->Map[i][j][2*Img->ZSize+k]=Img->Map[(i<<1)  ][(j<<1)+1][k];
     for(k=0;k<Img->ZSize;k++) Dst->Map[i][j][3*Img->ZSize+k]=Img->Map[(i<<1)+1][(j<<1)+1][k];
    }
   FreeMeABitMap3D(Img);
   return Dst;
  }
 
 BitMap3D *DeMortonOrder(BitMap3D *Img)
  {
   register unsigned int i,j,k,l;
   BitMap3D *Dst=GimmeABitMap3D(Img->XSize<<1,Img->YSize<<1,Img->ZSize>>2,Img->ImgType);
   
   for(i=0;i<Img->XSize;i++)
   for(j=0;j<Img->YSize;j++)
    {
     for(k=0;k<Dst->ZSize;k++) Dst->Map[(i<<1)  ][(j<<1)  ][k]=Img->Map[i][j][0*Dst->ZSize+k];
     for(k=0;k<Dst->ZSize;k++) Dst->Map[(i<<1)+1][(j<<1)  ][k]=Img->Map[i][j][1*Dst->ZSize+k];
     for(k=0;k<Dst->ZSize;k++) Dst->Map[(i<<1)  ][(j<<1)+1][k]=Img->Map[i][j][2*Dst->ZSize+k];
     for(k=0;k<Dst->ZSize;k++) Dst->Map[(i<<1)+1][(j<<1)+1][k]=Img->Map[i][j][3*Dst->ZSize+k];
    }
   FreeMeABitMap3D(Img);
   return Dst;
  }
 
 BitMap3D *PeanoOrder(BitMap3D *Img)
  {
   register unsigned int i,j,k,l;
   BitMap3D *Dst=GimmeABitMap3D(Img->XSize>>1,Img->YSize>>1,Img->ZSize<<2,Img->ImgType);
   static level=0;
   
   for(i=0;i<Dst->XSize;i++)
   for(j=0;j<Dst->YSize;j++)
    {
     for(k=0;k<Img->ZSize;k++) Dst->Map[i][j][0*Img->ZSize+k]=Img->Map[(i<<1)  ][(j<<1)  ][k];
     for(k=0;k<Img->ZSize;k++) Dst->Map[i][j][1*Img->ZSize+k]=Img->Map[(i<<1)+1][(j<<1)  ][k];
     for(k=0;k<Img->ZSize;k++) Dst->Map[i][j][2*Img->ZSize+k]=Img->Map[(i<<1)+1][(j<<1)+1][k];
     for(k=0;k<Img->ZSize;k++) Dst->Map[i][j][3*Img->ZSize+k]=Img->Map[(i<<1)  ][(j<<1)+1][k];
    }
   FreeMeABitMap3D(Img);
   return Dst;
  }
 
 BitMap3D *DePeanoOrder(BitMap3D *Img)
  {
   register unsigned int i,j,k,l;
   BitMap3D *Dst=GimmeABitMap3D(Img->XSize<<1,Img->YSize<<1,Img->ZSize>>2,Img->ImgType);
   
   for(i=0;i<Img->XSize;i++)
   for(j=0;j<Img->YSize;j++)
    {
     for(k=0;k<Dst->ZSize;k++) Dst->Map[(i<<1)  ][(j<<1)  ][k]=Img->Map[i][j][0*Dst->ZSize+k];
     for(k=0;k<Dst->ZSize;k++) Dst->Map[(i<<1)+1][(j<<1)  ][k]=Img->Map[i][j][1*Dst->ZSize+k];
     for(k=0;k<Dst->ZSize;k++) Dst->Map[(i<<1)+1][(j<<1)+1][k]=Img->Map[i][j][2*Dst->ZSize+k];
     for(k=0;k<Dst->ZSize;k++) Dst->Map[(i<<1) ][(j<<1)+1][k]=Img->Map[i][j][3*Dst->ZSize+k];
    }
   FreeMeABitMap3D(Img);
   return Dst;
  }
 
 void main(int argc,char *argv[])
  {
   char c;
   char *filename1,*filename2,*cmd,*dd;
   int block=2,depth=1,type=1,i;
   
   Quiet=FALSE;
   
   if (argc<4) MapUsage();
   while(--argc>3)
    {
     c=(*++argv)[0];
     if (!(c=='-')) ErrorHandler(UNKNOWN_OPTION,argv[0]);
     else
      {
       c=(*argv)[1];
       dd= &(*argv)[1];
       switch(c)
        {
         case 't': type=ReadInteger(&((*argv)[2]));
         break;
         case 'b': block=ReadInteger(&((*argv)[2]));
         break;
         case 'd': depth=ReadInteger(&((*argv)[2]));
         break;
         case 'q': Quiet=TRUE;
         break;
         default: ErrorHandler(UNKNOWN_OPTION,argv[0]);
         break;
        }
      }
    }
   
   cmd = &(*++argv)[0];
   filename1 = &(*++argv)[0];
   filename2 = &(*++argv)[0];
   
   c=cmd[0];
   switch(c)
    {
     case 'c': /* convert 2D image to 3D */
      {
       BitMap *Src;
       vpf(stderr,"Converting  bitmap '%s' to sequence '%s'...",filename1,filename2);
       Src=LoadBitMap(filename1,0,0,0,0,block,""); /* load image */
       
       switch(type)
        {
         case 1: /* Morton order */
          {
           BitMap *Tmp;
           BitMap3D *Dst;
           register int x,y,z,i,j,k=0;
           
           vpf(stderr,"\nMapping of 2D image to 3D using Morton Order...");
           x=Src->XSize;
           y=Src->YSize;
           z=depth;
           vprintf(stderr,"\n   Depth:%d",z);
           Dst=GimmeABitMap3D(x,y,1,Src->ImgType);
           BitMap2Frame(Src,Dst,0); /* copy frame to 3d map */
           FreeMeABitMap(Src);
           
           for(i=0;i<z;i++) 
            {
             vpf(stderr,"\n   Level:%d  Image:(%d,%d,%d)",i,Dst->XSize,Dst->YSize,Dst->ZSize);
             Dst=MortonOrder(Dst);
            }
           vpf(stderr,"\n   Level:%d  Image:(%d,%d,%d)",i,Dst->XSize,Dst->YSize,Dst->ZSize);
           SaveBitMap3D(filename2,Dst,"");
          }
         break;
         case 2: /* x subsample */
          {
           BitMap *Tmp;
           BitMap3D *Dst;
           register int x,y,z,i,j,k=0;
           
           vpf(stderr,"\nMapping of 2D image to 3D using x subsample...");
           x=Src->XSize/depth;
           y=Src->YSize;
           z=depth;
           vprintf(stderr,"\n   Depth:%d",depth);
           Dst=GimmeABitMap3D(x,y,z,Src->ImgType);
           
           for(i=0;i<x;i++) 
           for(j=0;j<y;j++) 
           for(k=0;k<z;k++) Dst->Map[i][j][k]=Src->Map[i*z+k][j];
           
           vpf(stderr,"\n   Level:%d  Image:(%d,%d,%d)",i,Dst->XSize,Dst->YSize,Dst->ZSize);
           SaveBitMap3D(filename2,Dst,"");
          }
         break;
         case 3: /* y subsample */
          {
           BitMap *Tmp;
           BitMap3D *Dst;
           register int x,y,z,i,j,k=0;
           
           vpf(stderr,"\nMapping of 2D image to 3D using y subsample...");
           x=Src->XSize;
           y=Src->YSize/depth;
           z=depth;
           vprintf(stderr,"\n   Depth:%d",depth);
           Dst=GimmeABitMap3D(x,y,z,Src->ImgType);
           
           for(i=0;i<x;i++) 
           for(j=0;j<y;j++) 
           for(k=0;k<z;k++) Dst->Map[i][j][k]=Src->Map[i][j*z+k];
           
           vpf(stderr,"\n   Level:%d  Image:(%d,%d,%d)",i,Dst->XSize,Dst->YSize,Dst->ZSize);
           SaveBitMap3D(filename2,Dst,"");
          }
         break;
         case 4: /* Peano order */
          {
           BitMap *Tmp;
           BitMap3D *Dst;
           register int x,y,z,i,j,k=0;
           
           vpf(stderr,"\nMapping of 2D image to 3D using Peano-Hilbert Order...");
           x=Src->XSize;
           y=Src->YSize;
           z=depth;
           vprintf(stderr,"\n   Depth:%d",z);
           Dst=GimmeABitMap3D(x,y,1,Src->ImgType);
           BitMap2Frame(Src,Dst,0); /* copy frame to 3d map */
           FreeMeABitMap(Src);
           
           for(i=0;i<z;i++) 
            {
             vpf(stderr,"\n   Level:%d  Image:(%d,%d,%d)",i,Dst->XSize,Dst->YSize,Dst->ZSize);
             Dst=PeanoOrder(Dst);
            }
           vpf(stderr,"\n   Level:%d  Image:(%d,%d,%d)",i,Dst->XSize,Dst->YSize,Dst->ZSize);
           SaveBitMap3D(filename2,Dst,"");
          }
         break;
         
         case 9: /* dum conversion - just plain collage */
          {
           BitMap *Tmp;
           BitMap3D *Dst;
           register int x,y,z,i,j,k=0;
           
           vpf(stderr,"\nMapping of 2D image to 3D using plain collage");
           
           x=(Src->XSize/block)*block/depth;
           y=(Src->YSize/block)*block/depth;
           z=depth<<2;
           
           Dst=GimmeABitMap3D(x,y,z,Src->ImgType);
           Tmp=GimmeABitMap(x,y,Src->ImgType);
           vpf(stderr,"New image size:(%d,%d,%d)",x,y,z);
           
           for(i=0;i<(Src->XSize/block)*block/x;i++)
           for(j=0;j<(Src->YSize/block)*block/y;j++)
            {
             Map2BitMap(Src,Tmp,i*x,j*y);
             BitMap2Frame(Tmp,Dst,k++);
            }
           SaveBitMap3D(filename2,Dst,"");
          }
         break;
         
         default:
         break;
         
        }
       vprintf(stderr,"\n");
       exit(0);
      }
     break;
     
     case 'r':  /* reconvert 3D image to 2D */
      {
       BitMap3D *Src;
       vpf(stderr,"\nConverting  sequence '%s' to bitmap '%s'...",filename1,filename2);
       Src=LoadBitMap3D(filename1,0,0,0,0,0,1,""); /* load image */
       
       switch(type)
        {
         case 1: /* Morton order */
          {
           BitMap *Dst;
           register int x,y,z,i,j,k=0;
           
           vpf(stderr,"\nRe-mapping of 3D image to 2D using Morton Order...");
           z=(int)(sqrt(Src->ZSize)+0.5)>>1;
           for(i=0;i<z;i++) 
            {
             vpf(stderr,"\n   Level:%d  Image:(%d,%d,%d)",i,Src->XSize,Src->YSize,Src->ZSize);
             Src=DeMortonOrder(Src);
            }
           vpf(stderr,"\n   Level:%d  Image:(%d,%d,%d)",i,Src->XSize,Src->YSize,Src->ZSize);
           Dst=GimmeABitMap(Src->XSize,Src->YSize,GRAYIMG);
           Frame2BitMap(Src,0,Dst);
           SaveBitMap(filename2,Dst,"");
          }
         break;
         case 2: /* x subsample */
          {
           BitMap *Dst;
           register int x,y,z,i,j,k=0;
           
           vpf(stderr,"\nRe-Mapping of 3D image to 2D using x subsample...");
           z=Src->ZSize;
           
           x=Src->XSize;
           y=Src->YSize;
           
           Dst=GimmeABitMap(x*z,y,Src->ImgType);
           
           for(i=0;i<x;i++) 
           for(j=0;j<y;j++) 
           for(k=0;k<z;k++) Dst->Map[i*z+k][j]=Src->Map[i][j][k];
           
           vpf(stderr,"\n   Image:(%d,%d)",Dst->XSize,Dst->YSize);
           SaveBitMap(filename2,Dst,"");
          }
         break;
         case 3: /* y subsample */
          {
           BitMap *Dst;
           register int x,y,z,i,j,k=0;
           
           vpf(stderr,"\nRe-Mapping of 3D image to 2D using y subsample...");
           z=Src->ZSize;
           
           x=Src->XSize;
           y=Src->YSize;
           
           Dst=GimmeABitMap(x,y*z,Src->ImgType);
           
           for(i=0;i<x;i++) 
           for(j=0;j<y;j++) 
           for(k=0;k<z;k++) Dst->Map[i][j*z+k]=Src->Map[i][j][k];
           vpf(stderr,"\n   Image:(%d,%d)",Dst->XSize,Dst->YSize);
           SaveBitMap(filename2,Dst,"");
          }
         break;
         case 4: /* Peano order */
          {
           BitMap *Dst;
           register int x,y,z,i,j,k=0;
           
           vpf(stderr,"\nRe-mapping of 3D image to 2D using Peano-Hilbert Order...");
           z=(int)(sqrt(Src->ZSize)+0.5)>>1;
           for(i=0;i<z;i++) 
            {
             vpf(stderr,"\n   Level:%d  Image:(%d,%d,%d)",i,Src->XSize,Src->YSize,Src->ZSize);
             Src=DePeanoOrder(Src);
            }
           vpf(stderr,"\n   Level:%d  Image:(%d,%d,%d)",i,Src->XSize,Src->YSize,Src->ZSize);
           Dst=GimmeABitMap(Src->XSize,Src->YSize,GRAYIMG);
           Frame2BitMap(Src,0,Dst);
           SaveBitMap(filename2,Dst,"");
          }
         
         default:
         break;
        }
       vprintf(stderr,"\n");
       exit(0);
      }
     break;
     
     
     
/*     case '2':  2d psnr 
      {
       BitMap *Src1=LoadBitMap(filename1,0,0,0,0,1,"");
       BitMap *Src2=LoadBitMap(filename2,0,0,0,0,1,"");
       vprintf(stderr,"\nPSNR between '%s' and '%s: %f [dB]",PSNR(Src1,Src2));
      }
     break;
*/
     case '3': /* 3d psnr */
      {
       BitMap3D *Src1=LoadBitMap3D(filename1,0,0,0,0,0,1,"");
       BitMap3D *Src2=LoadBitMap3D(filename2,0,0,0,0,0,1,"");
       vprintf(stderr,"\nPSNR between '%s' and '%s: %f [dB]",PSNR3D(Src1,Src2));
      }
     break;
     
     case 'p':  /* pgm file to ppm */
      {
       BitMap *Src;
       BitMap *Dst;
       register unsigned int i,j,k,l;
       
       vprintf(stderr,"Converting PGM file '%s' to PPM file '%s'...",filename1,filename2);
       
       Quiet=TRUE;
       Src=LoadBitMap(filename1,0,0,0,0,1,""); /* load image */
       if (Src->ImgType!=GRAYIMG) ErrorHandler(WRONG_FORMAT,"Not a PGM file");
       Dst=GimmeABitMap(Src->XSize,3*Src->YSize,RGBIMG);
       l=Src->YSize;
       
       for(i=0;i<Src->XSize;i++)
       for(j=0;j<Src->YSize;j++)
        {
         k=(Src->Map[i][j]);
         Dst->Map[i][j]    =k;
         Dst->Map[i][j+l]  =k;
         Dst->Map[i][j+l*2]=k;
        }
       
       SaveBitMap(filename2,Dst,"");
       
       vprintf(stderr,"\nDone!\n");
      }
     break;
    }
   
  }
 
 
