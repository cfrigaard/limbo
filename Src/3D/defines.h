/* defines.h */ 
 
/*** version ***/ 

#define LIMBOVERSION "4.0"

/*** error table ***/

#define NO_ERROR             0 /* pos error = fatal error */
#define INITIALIZATION_ERROR 1
#define NO_FREE_MEMORY       2 
#define UNKNOWN_VARIABLE     3
#define OUT_OF_RANGE         4
#define VAL_OUT_OF_RANGE     5
#define UNABLE_TO_OPEN       6
#define UNABLE_TO_CLOSE      7
#define ERROR_WRITING        8
#define ERROR_READING        9
#define FILEMODE_ERROR       10
#define FATAL_ERROR          11
#define RUNTIME_ERROR        12
#define UNKNOWN_ERROR        13
#define UNKNOWN_FORMAT       14
#define UNKNOWN_GFXFORMAT    15
#define LEARN_USAGE          16
 
#define UNKNOWN_OPTION       -1 /* neg error = nonfatal    */
#define NONFATAL_ERROR       -2
#define NULL_POINTER         -3
#define OUT_OF_RANGE2        -4
#define BITMAPSIZE_ERROR     -5
#define WRONG_FORMAT         -6
#define NOFILE               -7

/*** boolean ***/ 

#define ON    1 
#define OFF   0
#define TRUE  1
#define FALSE 0

/*** transformations ***/

#define ABSORBGRAY      1
#define LUMINANCESHIFT  2
#define CONTRASTSCALE   3

#define NOBLOCK         0
#define EDGEBLOCK       1
#define SHADEBLOCK      2

/*** classify and analyze ***/

#define MEAN        0
#define VAR         1
#define STDDEV      2

#define EDGEBLOCK_COLOR     180
#define SHADEBLOCK_COLOR    220
 
#define F2LONG              10000.0
#define PREDEFFEATURES      3
#define POSTLISTEXTRA       40

/*** code types ***/

#define UNDEFINED       0
#define GRAYIMG         1
#define RGBIMG          2
#define YUVIMG          3
#define GRAYANIM        4
#define RGBANIM         5
#define YUVANIM         6

/*** fsm fileio ***/

#define LIMBOSTRINGSIZE 11

#define FILE_BUFFERSIZE 1024

#define FILE_CLOSED     0
#define FILE_OPENREAD   1
#define FILE_OPENWRITE  2
#define FILE_OPENAPPEND 3
#define FILE_APPEND     4
#define FILE_WRITE      5
#define FILE_READ       6
#define FILE_CLOSE      7
 
/*** File directories ***/

#define ORGGFX "OrgGfx/"
#define FCCODE "FCCodes/"
#define CMPGFX "CmpGfx/"
#define ENTROPY "Data/"

#define MAXFILE     255 /* max file name */
#define MAXSTRING   255 /* max string size */
#define MAXARRAY    65535 /* max array size */

#define PGMSFX      ".pgm"
#define PGMMAGIC    "P5\0"
#define PPMSFX      ".ppm"
#define PPMMAGIC    "P6\0"

#define VIEWER      "xv"
#define ANIMVIEWER  "xv -wait 1 -wloop"
#define RUN         "&"

#define vprintf if (!Quiet) fprintf
#define vpf     if (!Quiet) fprintf