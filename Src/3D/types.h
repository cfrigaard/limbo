/* types.h */

typedef struct bitmap
 {
  int XSize;
  int YSize;
  int ImgType;
  unsigned char **Map;
 }
BitMap;

typedef struct bitmap3d
 {
  int XSize;
  int YSize;
  int ZSize;
  int ImgType;
  unsigned char ***Map;
 }
BitMap3D;

typedef struct poolnode
 {
  int x,y,z;
  float *Features;
  long  *Distance;
  int   *Index;
  struct poolnode **Next;
 } 
PoolNode;

typedef struct grid
 {
   struct grid **Next;
 } 
Grid;

typedef struct gridterm
 {
  unsigned long Count;
  struct listnode *Class;
 } 
GridTerm;

typedef struct featurespace
 {
   int Dimension,Size;
   unsigned long Count;
   struct listnode *Class;
   struct grid *GridArray;
 } 
FeatureSpace;

typedef struct listnode
 {
  int    Info;
  unsigned long L;
  struct poolnode *Domain;
  struct listnode *Next;
  struct listnode *Pred;
 }
ListNode;

typedef struct poolstructure
 {
  int D,B;
  int DomainX,DomainY,DomainZ;
  int RangeX,RangeY,RangeZ;
  struct poolnode ****DomainNodes;
  struct poolnode ****RangeNodes;
  struct featurespace *DomainSpace;
  struct bitmap3d *SampledDomainBitmap3D;
  struct poolstructure *Next;
 }   
PoolStructure;

typedef struct transformation
 {
  int Type;
  int D;
  int BlockSize;
  int g0;
  int tn;
  float Alpha;
  int Deltag;
  unsigned long Distortion;
  PoolNode *Domain;
  struct transformation ****Sub;
 }
Transformation;

typedef struct parameter
 {
  int TMainSub,TShadeEdge,TPostProcess;
  int XBits,YBits,ZBits,SBits,ABits,DBits,GBits,TBits;
  float AMax,AMin;
 }
Parameter;

typedef struct limboheader
 {
  unsigned char HeadSize;
  unsigned char Type;
  unsigned char MSBx;
  unsigned char LSBx;
  unsigned char MSBy;
  unsigned char LSBy;
  unsigned char MSBz;
  unsigned char LSBz;
  unsigned char MSBox;
  unsigned char LSBox;
  unsigned char MSBoy;
  unsigned char LSBoy;
  unsigned char MSBoz;
  unsigned char LSBoz;

  unsigned char Col;
  unsigned char B;
  unsigned char Delta;
  unsigned char NSquare;

  unsigned char XBits;
  unsigned char YBits;
  unsigned char ZBits;
  unsigned char SBits;
  unsigned char ABits;
  unsigned char AMax;
  unsigned char AMin;
  unsigned char DBits;
  unsigned char GBits;
  unsigned char TBits;

  unsigned char MSBSNR;
  unsigned char LSBSNR;
  unsigned char MSBRatio;
  unsigned char LSBRatio;
  unsigned char LSBCPU;
  unsigned char MSBCPU;
 }
LimboHeader;

extern int Quiet;  /* extern verbose variable */
