 void Map2BitMap(BitMap *SrcImg,BitMap *DstImg,int XStart,int YStart)
  ;
 void BitMap2Map(BitMap *SrcImg,BitMap *DstImg,int XStart,int YStart)
  ;
 void Map3D2BitMap3D(BitMap3D *SrcImg,BitMap3D *DstImg,int XStart,int YStart,int ZStart)
  ;
 void CopyBitMap(BitMap *SrcImg,BitMap *DstImg)
  ;
 void BitMap3D2Map3D(BitMap3D *SrcImg,BitMap3D *DstImg,int XStart,int YStart,int ZStart)
  ;
 void CopyBitMap3D(BitMap3D *SrcImg,BitMap3D *DstImg)
  ;
 void Frame2BitMap(BitMap3D *SrcImg,int z,BitMap *DstImg)
  ;
 void BitMap2Frame(BitMap *SrcImg,BitMap3D *DstImg,int z)
  ;
 void T_CScaleAndLShift(BitMap *SrcImg,float Scale,int Shift)
  ;
 void T_AbsorbGrey(BitMap *SrcImg, unsigned char Par)
  ;
 void Sample(BitMap *SrcImg,BitMap *DstImg,int XPos,int YPos)
  ;
 BitMap *Expand2(BitMap *SrcImg)
  ;
 float PSNR(BitMap *ImgA,BitMap *ImgB)
  ;
 unsigned long dl2(BitMap *ImgA,int AXPos,int AYPos,BitMap *ImgB)
  ;
 void Isometri(BitMap *SrcImg,BitMap *DstImg, int type)
  ;
 unsigned long IsoAnddl2(BitMap *ImgA, int AXPos, int AYPos,
                         BitMap *ImgB, int Size, int type)
  ;
 unsigned char MakeRange(float p,Parameter *pa) /* asymetric quant */
  ;
 float MakeFloat(unsigned char conv,Parameter *pa)
  ;
