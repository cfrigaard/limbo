/*
 compile and link command:  sc link pgm2ppm.c  bitmapio.o allocate.o etc.o mapop.o
*/

#include "includes.h"

int Quiet=FALSE;

/**************************************|****************************************
Routine   : main
Input  par: int argc (argument count)
            char *argv[] (pointer to arguments)
Output par: none
Function  : converts a pgm image to a ppm
***************************************|***************************************/

 int main(int argc,char *argv[])
  {
   char filename[999];
   register unsigned int x,y,c;
   BitMap *DstImg,*Image;

   if (argc!=2)
    {
     vprintf(stderr,"Usage: pgm2ppm <file>\nVersion: 0.0\n");
     exit(0);
    }

   strcpy(filename,argv[1]);
   vprintf(stderr,"Converting...File:'%s'",filename);

   Image=LoadBitMap(filename,0,0,0,0,1,"");

   if (Image==NULL)
    {
     vprintf(stderr,"\nImage not found!\n");
     exit(1);
    }
   vprintf(stderr,"\ntype:%d",Image->ImgType);

   if (Image->ImgType!=GRAYIMG)
    {
     vprintf(stderr,"Not a graylevel image...aborting!\n");
     exit(10);
    }

   DstImg=GimmeABitMap(Image->XSize,Image->YSize*3,RGBIMG);

   for(y=0;y<Image->YSize;y++)
    {
     for(x=0;x<(Image->XSize);x++)
      {
       c=Image->Map[x][y];
       DstImg->Map[x][y] = c;
       DstImg->Map[x][y+Image->YSize] = c;
       DstImg->Map[x][y+2*Image->YSize] = c;
      }
    }

   vprintf(stderr,"\nSaving ppm image...");
   strcpy(filename,argv[1]);
   SaveBitMap(filename,DstImg,"");
   vprintf(stderr,"\nDone!\n");

   return 0;
 }

