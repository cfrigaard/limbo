 void Color(BitMap *map,unsigned char col)
  ;
 void Color3D(BitMap3D *map,unsigned char col)
  ;
 BitMap *Gen3D(BitMap3D *Org,int thres, int step,
               int framecol, int stepframecol,int background)
  ;
 void Rect(BitMap *map,int x, int y, int dx, int dy, int col)
  ;
 void Line(BitMap *map, int x1, int y1, int x2, int y2, int col)
  ;
