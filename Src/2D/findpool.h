 int GridQuant(float Dist, int Size)
  ;
 PoolNode ***FindNodes(BitMap *Image,PoolStructure *Pool,int B,int delta,
                       int FeatureDim,float fTShadeEdge, int domain)
  ;
 FeatureSpace *FindSpace(PoolStructure *Pool,int FeatureDim,int Size,float fTShadeEdge)  
  ;
 PoolStructure *FindPool(BitMap *Image,int FeatureDimensions, int GridRes,
                         int TShadeEdge, int NSquare,int B,int delta)
  ;
