#include "includes.h"
 
/**************************************|****************************************
Routine   : LearnUsage
Input  par: none
Output par: none
Global var: none
Function  : Prints out program-usage.
***************************************|***************************************/
 
 void LearnUsage(void)
  {
   printf("\nLimbo version %s - 2D\n",LIMBOVERSION);
   printf("\nUsage: limbo2d [-options] <command> <file>");
   printf("\n\nCommands:");
   printf("\n e     Encode image");
   printf("\n d     Decode fractal codes");
   printf("\n v     View fractal code informations");
   printf("\n a     Analyze fractal code");
   printf("\n\nOptions:\n");
   printf("\n -x<n>    Maximum 'x' size");
   printf("\n -y<n>    Maximum 'y' size");
   printf("\n -ox<n>   Offset 'x' ");
   printf("\n -oy<n>   Offset 'y'");
   printf("\n -b<n>    Range block size (min 2)");
   printf("\n -d<n>    Delta step");
   printf("\n -n<n>    Use n-level square encoding");
   printf("\n -m<n>    Minimum domain-block list size");
   printf("\n -s<n>    Maximum domain-block list size");
   printf("\n -f<n>    Feature space dimensions");
   printf("\n -r<n>    Feature grid resolution");
   printf("\n -l<n>    Expand decoding at 'l' levels");
   printf("\n -i<n>    Decode iterations");
   printf("\n\n -Qa<n>   Alpha  quant bit");
   printf("\n -Qd<n>   DeltaG quant bit (without a sign bit)");
   printf("\n -Amin<f> Alpha minimum value");
   printf("\n -Amax<f> Alpha maximum value (max. 2.55)");
   printf("\n -Tm<n>   Threshold for main/sub blocks");
   printf("\n            (breakup blocks wiht dl2 greater than <n>)");
   printf("\n -Te<n>   Threshold for shade/edge");
   printf("\n            (make blocks edges with variance greater than <n>)");
   printf("\n -Tp<n>   Threshold for postprocessing of blocks");
   printf("\n            (process blocks with dl2 greater than <n>)");
   printf("\n\n -q    Be quiet");
/*   printf("\n\nEnvironment variables:\n");
   printf("\n Limbo_OrgGfx  = Original graphics files");
   printf("\n Limbo_CmpGfx  = Compare (output) graphics files");
   printf("\n Limbo_FCCodes = Fractal codes in limbo-format");
   printf("\n Limbo_Entropy = Entropy data files"); */
   printf("\n\n");
   MyExit(0);
  }
 
/**************************************|****************************************
Routine   : ErrorHandler
Input  par: int error     (error number pos=fatal neg=nonfatal) 
            char *message (pointer to specific error message string)
Output par: none
Global var: none
Function  : Prints errorname and specific error message to stderr. 
            If error pos then MyExit is called.
***************************************|***************************************/
 
 void ErrorHandler(int error,char *message)
  {
   char msg[MAXSTRING];
   switch(error)
    {
     case NO_ERROR : strcpy(msg,"No Error");
     break;
     case INITIALIZATION_ERROR : strcpy(msg,"Initialization error");
     break;
     case NO_FREE_MEMORY: strcpy(msg,"No free memory");
     break;
     case UNKNOWN_VARIABLE : strcpy(msg,"Unknown variable");
     break;
     case OUT_OF_RANGE: strcpy(msg,"Out of range");
     break;
     case VAL_OUT_OF_RANGE : strcpy(msg,"Value out of range");
     break;
     case UNABLE_TO_OPEN : strcpy(msg,"Unable to open");
     break;
     case UNABLE_TO_CLOSE: strcpy(msg,"Unable to close");
     break;
     case ERROR_WRITING : strcpy(msg,"Error writing");
     break;
     case ERROR_READING: strcpy(msg,"Error reading");
     break;
     case FILEMODE_ERROR: strcpy(msg,"Filemode error");
     break;
     case FATAL_ERROR: strcpy(msg,"Fatal error");
     break;
     case RUNTIME_ERROR: strcpy(msg,"Runtime error");
     break;
     case UNKNOWN_ERROR: strcpy(msg,"Unknown error");
     break;
     case UNKNOWN_FORMAT : strcpy(msg,"Not in reconizable Limbo format");
     break;
     case UNKNOWN_GFXFORMAT : strcpy(msg,"Not in reconizable graphic format");
     break;
     case UNKNOWN_OPTION : strcpy(msg,"Unknown option");
     break;
     case NONFATAL_ERROR : strcpy(msg,"Nonfatal error");
     break;
     case NULL_POINTER : strcpy(msg,"Null pointer");
     break;
     case OUT_OF_RANGE2 : strcpy(msg,"Out of range");
     break;
     case BITMAPSIZE_ERROR : strcpy(msg,"Bitmapsize error");
     break;
     case WRONG_FORMAT : strcpy(msg,"Wrong format");
     break;
     case NOFILE : strcpy(msg,"No file");
     break;
     case LEARN_USAGE: 
      {
       LearnUsage();
       MyExit(LEARN_USAGE);
      }
     break;
     default:strcpy(msg,"Ooups - error not in errortable");
     break;
    }
   fprintf(stderr,"\n%s: %s\n",msg,message);
   if (error>0) MyExit(error); /* fatal error */
  }
 
/**************************************|****************************************
Routine   : SaveAndView
Input  par: char *fil (filename without any extension)
            BitMap *map (bitmap to save)
Output par: none
Global var: none
Function  : Saves bitmap, converts it from 'hips' to 'pgm', views it by 'xv'
            (unix only).
***************************************|***************************************/
 
 void SaveAndView(char *fil, BitMap *map)
  {
   char syscmd[MAXARRAY],file[MAXARRAY];
   
   SaveBitMap(fil,map,CMPGFX);
   
   strcpy(syscmd,VIEWER); /* viewer */
   strcat(syscmd," ");
   strcpy(file,CMPGFX);
   strcat(file,fil);
   strcat(syscmd,file);
   if (map->ImgType==GRAYIMG) strcat(syscmd,PGMSFX);
   if (map->ImgType==RGBIMG) strcat(syscmd,PPMSFX);
   strcat(syscmd," ");
   strcat(syscmd,RUN);
   vprintf(stderr,"\nViewing...\n   File:'%s'",file);
   system(syscmd);
  }
 
 void SaveAndView3D(char *fil, BitMap3D *map)
  {
   char syscmd[MAXARRAY],file[MAXARRAY];

   SaveBitMap3D(fil,map,CMPGFX);
   
   strcpy(syscmd,ANIMVIEWER); 
   strcat(syscmd," ");
   strcpy(file,CMPGFX);
   strcat(file,fil);
   strcat(syscmd,file);
   if (map->ImgType==GRAYIMG) strcat(syscmd,PGMSFX);
   if (map->ImgType==RGBIMG) strcat(syscmd,PPMSFX);  
   strcat(syscmd," ");
   strcat(syscmd,RUN);
   vprintf(stderr,"\nViewing...\n   File:'%s'",file);
   /*system(syscmd);  */
  }
 
/**************************************|****************************************
Routine   : Smallies
***************************************|***************************************/
 
 void MyExit(int errno)
  {
   exit(errno);
  }
 
 int ReadInteger(char *convstring)
  {
   int temp=atoi(convstring);
   if (temp<0) temp = -temp;       /* only read positive ints */
   return(temp);
  }
 
 float ReadFloat(char *convstring)
  {
   float temp=atof(convstring);
   if (temp<0.0) temp = -temp;     /* only read positive floats */
   return(temp);
  }
 
 char *GetEnv(char *environment)
  {
   char *env=getenv(environment);
   if (env==NULL) strcpy(env," ");
   return  env;
  }
 
 unsigned char Invers(unsigned char x)
  {
   if (x>127) return 0;
   else return 255;
  }
