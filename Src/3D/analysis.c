#include "includes.h"

/**************************************|****************************************
Routine   : CountType
Input  par: Transformation *T (pointer to trans)
            int type          (type to be counted)
Output par: int               (counts)
Function  : counts main-block and its subblocs of type 'type'. Does the counting
            recursivly. 
***************************************|***************************************/
 
 unsigned long CountType3D(Transformation *T, int type)
  {
   unsigned long count=0,i,j,k;
   
   if (T->Type==type) count++;
   if (T->Sub!=NULL)
    {
     for(i=0;i<2;i++)
     for(j=0;j<2;j++)
     for(k=0;k<2;k++) if (T->Sub[i][j][k]!=NULL) count += CountType3D(T->Sub[i][j][k],type);
    }
   return count;
  }
 
/**************************************|****************************************
Routine   : CountMainSubs
Input  par: Transformation *T (pointer to trans)
Output par: unsigned int (counted mainsub configurations)
Function  : counts main-sub-configurations recursively.
***************************************|***************************************/
 
 unsigned long CountMainSubs3D(Transformation *T,unsigned long *PureMain,int level)
  {
   unsigned long count=1,i,j,k;   

   if (T->Sub!=NULL)
    {
     for(i=0;i<2;i++)
     for(j=0;j<2;j++)
     for(k=0;k<2;k++) 
     if (T->Sub[i][j][k]!=NULL) 
     { 
       level--;
       count +=  CountMainSubs3D(T->Sub[i][j][k],PureMain,level);
      }
    }
   else (PureMain[level])++;
   return count;
  }
 
/**************************************|****************************************
Routine   : HeadInfo
Input  par: Transformation ***T (pointer to trans array)
            LimboHeader *Head   (pointer to header structure)
Output par: none
Function  : Produce info-statistic on Limbo-Header (fractal code header).
***************************************|***************************************/
 
 void HeadInfo3D(Transformation ****T, LimboHeader *Head)
  {
   Parameter *Par=GimmeAParameter();
   int i,j,k,x,y,z;
   unsigned long edg=0,sha=0,mainsubs=0;
   unsigned long *PureMain;
   
   vprintf(stderr,"\nFractal code info...");
   
   Par->XBits=Head->XBits;
   Par->YBits=Head->YBits;
   Par->ZBits=Head->ZBits;
   
   Par->SBits=Head->SBits;
   Par->ABits=Head->ABits;
   Par->AMax =Head->AMax/100.0;
   Par->AMin =Head->AMin/100.0;
   Par->DBits=Head->DBits;
   Par->GBits=Head->GBits;
   Par->TBits=Head->TBits;
   
   x=Head->MSBx*256+Head->LSBx;
   y=Head->MSBy*256+Head->LSBy;
   z=Head->MSBz*256+Head->LSBz;
   
   PureMain=GimmeALongArray(Head->NSquare+1);
   for (i=0;i<Head->NSquare;i++) PureMain[i]=0;

   for (i=0;i<x/(Head->B<<Head->NSquare);i++) /* count edges + shades */
   for (j=0;j<y/(Head->B<<Head->NSquare);j++)
   for (k=0;k<z/(Head->B<<Head->NSquare);k++)
    {
     edg += CountType3D(T[i][j][k],EDGEBLOCK);
     sha += CountType3D(T[i][j][k],SHADEBLOCK);
     mainsubs += CountMainSubs3D(T[i][j][k],PureMain,Head->NSquare);
    }
   
    {
     unsigned long he=(sizeof(LimboHeader)+LIMBOSTRINGSIZE)*8;
     unsigned long edgbit=1+Par->XBits+Par->YBits+Par->ZBits+Par->ABits+Par->DBits+1+Par->TBits;
     unsigned long shabit=1+Par->GBits;
     unsigned long ed=edg*edgbit;
     unsigned long sh=sha*shabit;
     unsigned long to=ed+sh+Par->SBits*mainsubs;
     unsigned long ratio=(x*y*z*8*100)/to;
     unsigned long SNR=Head->MSBSNR*256+Head->LSBSNR;
     unsigned long cpu=Head->MSBCPU*256+Head->LSBCPU;
     
     vprintf(stderr,"\n   Size:(%d,%d,%d)  Offset:(%d,%d)",x,y,z,
             Head->MSBox*256+Head->LSBox,Head->MSBoy*256+Head->LSBoy);
     for (i=Head->NSquare;i>=0;i--) 
     vprintf(stderr,"\n   Block size:  Domain:%2d  Range:%2d  Delta:%2d  Pure main blocks:%d",
             Head->B<<(1+i),Head->B<<i,Head->Delta,PureMain[i]);
/*
     vprintf(stderr,"\n   Type:%d  Colors:%d  KeepDelta:%d ",
             Head->Type,Head->Col,Head->KeepDelta,Head->Type);
*/
     vprintf(stderr,"\n   Bits:  X:%d  Y:%d  Z:%d  Sub:%d  Alpha:%d  DeltaG:%d  G0:%d  TN:%d",
             Par->XBits,Par->YBits,Par->ZBits,Par->SBits,Par->ABits,Par->DBits,
             Par->GBits,Par->TBits);
     vprintf(stderr,"\n   Range:  Alpha max:%1.2f  Alpha min:%1.2f",Par->AMax,Par->AMin);
     vprintf(stderr,"\n   Header:                                         %7d [bit]",he);
     vprintf(stderr,"\n   Mainsub: %9u blocks of size %2u [bit] = %10u [bit]",
             mainsubs,Par->SBits,mainsubs*Par->SBits);
     vprintf(stderr,"\n   Shades:  %9u blocks of size %2u [bit] = %10u [bit]",sha,shabit,sh);
     vprintf(stderr,"\n   Edges:   %9u blocks of size %2u [bit] = %10u [bit]",edg,edgbit,ed);
     vprintf(stderr,"\n   Total:                                           %u [bit] = %u [byte]",
             to+he,(to+he+7)/8);
     vprintf(stderr,"\n   Ratio:   %3.2f:1 = %3.2f%% = %1.3f [bpp]",
             (float)ratio/100.0,10000.0/(float)ratio,(float)to/(float)(x*y*z));
     vprintf(stderr,"\n   PSNR:    %3.2f [dB]\n   CPU Time:%5u [sec]",(float)SNR/100.0,cpu);
    }

   FreeMeAULongArray(PureMain);
  }
 
